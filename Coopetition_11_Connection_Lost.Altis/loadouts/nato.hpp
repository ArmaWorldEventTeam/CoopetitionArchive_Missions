class PistolBaseWest : BaseCollection {
    handgun[] = {"hgun_Pistol_heavy_01_F"};
    handgunLoadedMagazine[] = {"11Rnd_45ACP_Mag"};
    magazines[] = {{"11Rnd_45ACP_Mag", 2}};
};

class BaseClassWest : BaseCollection {
    uniform[] = {"U_BG_Guerilla2_1", "U_I_G_Story_Protagonist_F"};
    vest[] = {"V_PlateCarrier2_rgr", "V_PlateCarrier1_rgr"};
    headgear[] = {"H_HelmetSpecB_paint1", "H_HelmetSpecB_sand", "H_HelmetSpecB_paint2", "H_HelmetSpecB_snakeskin", "H_HelmetB_light_desert"};
    backpack[] = {"B_Kitbag_cbr"};

    primaryWeapon[] = {"arifle_MX_Black_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_black_mag_Tracer"};
    primaryWeaponBarrel[] = {"ACE_acc_pointer_green"};
    primaryWeaponOptic[] = {"optic_Holosight"};

    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch"};

    class Pistol : PistolBaseWest {};
    class Smoke : BaseGrenades {};
    class ACEItems: BaseACEItems {};
    class ACRERadio: Radio343 {};
};

class LeaderBaseWest : BaseClassWest {
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch", "ItemGPS"};
};

class B_officer_F : LeaderBaseWest {
    headgear[] = {"H_Cap_headphones"};
    primaryWeapon[] = {"arifle_MXC_Black_F"};
    binocular[] = {"Laserdesignator"};
    magazines[] = {{"Laserbatteries",1},{"30Rnd_65x39_caseless_black_mag_Tracer",SPECIAL_AMMO}};
    items[] = {{"H_HelmetB_light_desert",1},{"ACRE_PRC148",1},{"ACRE_PRC117F",1}};
};

class B_Soldier_SL_F : LeaderBaseWest {
    primaryWeapon[] = {"arifle_MX_GL_Black_F"};
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_65x39_caseless_black_mag_Tracer",STANDARD_AMMO}};
    items[] = {{"ACRE_PRC148",1}};
    class SmokeUGL : BaseUGLSmokes {};
};

class B_Soldier_TL_F : LeaderBaseWest {
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_65x39_caseless_black_mag_Tracer",STANDARD_AMMO}};
};

class B_Soldier_GL_F : BaseClassWest {
    primaryWeapon[] = {"arifle_MX_GL_Black_F"};
    magazines[] = {{"30Rnd_65x39_caseless_black_mag_Tracer",STANDARD_AMMO}};
    class SmokeUGL : BaseUGLSmokes {};
    class UGL : BaseUGLs {};
};

class B_soldier_AR_F : BaseClassWest {
    primaryWeapon[] = {"LMG_Mk200_F"};
    primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box_Tracer_Red"};
    binocular[] = {"Binocular"};
    magazines[] = {{"200Rnd_65x39_cased_Box_Tracer_Red", /*MG_AMMO-1*/ 3}};
};

class B_HeavyGunner_F : BaseClassWest {
    primaryWeapon[] = {"MMG_02_black_F"};
    primaryWeaponResting[] = {"bipod_01_F_blk"};
    primaryWeaponLoadedMagazine[] = {"130Rnd_338_Mag"};
    magazines[] = {{"130Rnd_338_Mag",MMG_AMMO}};
};

class B_soldier_AAR_F : BaseClassWest {
    magazines[] = {{"30Rnd_65x39_caseless_black_mag_Tracer",SPECIAL_AMMO},{"130Rnd_338_Mag",/*MMG_AMMO-1*/3},{"ACE_SpareBarrel",1}};
    binocular[] = {"Binocular"};
};

class B_soldier_M_F : BaseClassWest {
    primaryWeapon[] = {"srifle_EBR_F"};
    primaryWeaponOptic[] = {"optic_mrco"};
    primaryWeaponLoadedMagazine[] = {"20Rnd_762x51_Mag"};
    binocular[] = {"Binocular"};
    magazines[] = {{"20Rnd_762x51_Mag", MRK_AMMO}};
};

class B_Soldier_F : BaseClassWest {
    magazines[] = {{"30Rnd_65x39_caseless_black_mag_Tracer", RIFLEMAN_AMMO}};
};

class CfgCLibLoadoutsClassBase; // Import Base Class
class CfgCLibLoadouts {
    class BaseCollection : CfgCLibLoadoutsClassBase  {
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    class BaseUGLs : BaseCollection {
        magazines[] = {{"1Rnd_HE_Grenade_shell",8}};
    };

    class BaseUGLSmokes : BaseCollection {
        magazines[] = {{"1Rnd_SmokeRed_Grenade_shell",5},{"1Rnd_SmokeGreen_Grenade_shell",5},{"1Rnd_SmokeOrange_Grenade_shell",5}};
    };

    class BaseGrenades : BaseCollection {
        magazines[] = {{"SmokeShell",2},{"HandGrenade",1},{"MiniGrenade",1},{"SmokeShellRed",1},{"SmokeShellGreen",1},{"SmokeShellOrange",1}};
    };

    class BaseACEItems : BaseCollection {
        items[] = {{"ACE_tourniquet",2},{"ACE_EarPlugs",1},{"ACE_fieldDressing",5},{"ACE_quikclot",5},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_CableTie",2}};
    };
    class Radio343 : BaseCollection {
        items[] = {{"ACRE_PRC343", 1}};
    };

    #define RIFLEMAN_AMMO 10
    #define STANDARD_AMMO 8
    #define SPECIAL_AMMO 6
    #define MG_AMMO 4
    #define MMG_AMMO 4
    #define MRK_AMMO 9

    #include "nato.hpp"
    #include "opfor.hpp"
    #include "guer.hpp"

    // Zeus
    class C_Soldier_VR_F : BaseCollection {
        headgear[] = {"H_Hat_Tinfoil_F"};
        binocular[] = {"Binocular"};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch", "ItemGPS"};
    };
};

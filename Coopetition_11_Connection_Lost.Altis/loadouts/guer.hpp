class PistolBaseGUER : BaseCollection {
    handgun[] = {"hgun_ACPC2_F"};
    handgunLoadedMagazine[] = {"9Rnd_45ACP_Mag"};
    magazines[] = {{"9Rnd_45ACP_Mag", 2}};
};

class BaseClassGUER : BaseCollection {
    uniform[] = {"U_BG_leader", "U_BG_Guerrilla_6_1", "U_BG_Guerilla1_1", "U_I_G_resistanceLeader_F"};
    vest[] = {"V_PlateCarrierIA1_dgtl"};
    headgear[] = {"H_PASGT_basic_black_F", "H_PASGT_basic_olive_F", "H_HelmetIA"};
    backpack[] = {"B_TacticalPack_oli"};

    primaryWeapon[] = {"arifle_TRG21_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
    primaryWeaponBarrel[] = {"ACE_acc_pointer_green"};
    primaryWeaponOptic[] = {"optic_Aco"};

    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch"};

    class Pistol : PistolBaseGUER {};
    class Smoke : BaseGrenades {};
    class ACEItems: BaseACEItems {};
    class ACRERadio: Radio343 {};
};

class LeaderBaseGUER : BaseClassGUER {
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch", "ItemGPS"};
};

class I_officer_F : LeaderBaseGUER {
    headgear[] = {"H_Cap_headphones"};
    primaryWeapon[] = {"arifle_TRG20_F"};
    binocular[] = {"Laserdesignator"};
    magazines[] = {{"30Rnd_556x45_Stanag_Tracer_Yellow",SPECIAL_AMMO},{"Laserbatteries",1}};
    items[] = {{"ACRE_PRC148",1},{"ACRE_PRC117F",1}, {"H_PASGT_basic_black_F",1}};
};

class I_Soldier_SL_F : LeaderBaseGUER {
    primaryWeapon[] = {"arifle_TRG21_GL_F"};
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_556x45_Stanag_Tracer_Yellow",STANDARD_AMMO}};
    items[] = {{"ACRE_PRC148",1}};
    class SmokeUGL : BaseUGLSmokes {};
};

class I_Soldier_TL_F : LeaderBaseGUER {
    headgear[] = {"H_Cap_headphones"};
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_556x45_Stanag_Tracer_Yellow",STANDARD_AMMO}};
};

class I_Soldier_GL_F : BaseClassGUER {
    primaryWeapon[] = {"arifle_TRG21_GL_F"};
    magazines[] = {{"30Rnd_556x45_Stanag_Tracer_Yellow",STANDARD_AMMO}};
    class SmokeUGL : BaseUGLSmokes {};
    class UGL : BaseUGLs {};
};

class I_Soldier_AR_F : BaseClassGUER {
    primaryWeapon[] = {"LMG_Mk200_F"};
    primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box_Tracer"};
    magazines[] = {{"200Rnd_65x39_cased_Box_Tracer",MG_AMMO}};
};

class I_support_MG_F : BaseClassGUER {
    primaryWeapon[] = {"MMG_01_tan_F"};
    primaryWeaponResting[] = {"bipod_02_F_tan"};
    magazines[] = {{"150Rnd_93x64_Mag",MMG_AMMO}};
};

class I_Soldier_AAR_F : BaseClassGUER {
    backpack[] = {"B_Carryall_oli"};
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_556x45_Stanag_Tracer_Yellow", SPECIAL_AMMO},{"150Rnd_93x64_Mag",/*MMG_AMMO-1*/3},{"ACE_SpareBarrel",1}};
};

class I_Soldier_M_F : BaseClassGUER {
    primaryWeapon[] = {"srifle_EBR_F"};
    primaryWeaponOptic[] = {"optic_mrco"};
    primaryWeaponLoadedMagazine[] = {"20Rnd_762x51_Mag"};
    binocular[] = {"Binocular"};
    magazines[] = {{"20Rnd_762x51_Mag", MRK_AMMO}};
};

class I_soldier_F : BaseClassGUER {
    magazines[] = {{"30Rnd_556x45_Stanag_Tracer_Yellow", RIFLEMAN_AMMO}};
};

class I_Soldier_exp_F : BaseClassGUER {
    magazines[] = {{"30Rnd_556x45_Stanag_Tracer_Yellow",SPECIAL_AMMO},{"DemoCharge_Remote_Mag",5}};
    items[] = {{"ACE_Clacker",1},{"ACE_DefusalKit",1}};
};

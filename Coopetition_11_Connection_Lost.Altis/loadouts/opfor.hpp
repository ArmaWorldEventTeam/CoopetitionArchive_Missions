class PistolBaseEast : BaseCollection {
    handgun[] = {"hgun_Rook40_F"};
    handgunLoadedMagazine[] = {"16Rnd_9x21_Mag"};
    magazines[] = {{"16Rnd_9x21_Mag", 2}};
};

class BaseClassEast : BaseCollection {
    uniform[] = {"U_O_CombatUniform_ocamo"};
    vest[] = {"V_TacVest_brn"};
    headgear[] = {"H_HelmetO_ocamo"};
    backpack[] = {"B_TacticalPack_ocamo"};

    primaryWeapon[] = {"arifle_Katiba_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
    primaryWeaponOptic[] = {"optic_ACO_grn"};
    primaryWeaponBarrel[] = {"acc_pointer_IR"};

    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch"};

    class Pistol : PistolBaseEast {};
    class Smoke : BaseGrenades {};
    class ACEItems: BaseACEItems {};
    class ACRERadio: Radio343 {};
};

class LeaderBaseEast : BaseClassEast {
    primaryWeapon[] = {"arifle_Katiba_C_F"};
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch", "ItemGPS"};
};

class O_officer_F : LeaderBaseEast {
    headgear[] = {"H_MilCap_ocamo"};
    binocular[] = {"Laserdesignator"};
    magazines[] = {{"30Rnd_65x39_caseless_green_mag_Tracer",SPECIAL_AMMO},{"Laserbatteries",1}};
    items[] = {{"H_HelmetO_ocamo",1},{"ACRE_PRC117F",1},{"ACRE_PRC148",1}};
};

class O_Soldier_SL_F : LeaderBaseEast {
    headgear[] = {"H_HelmetLeaderO_ocamo"};
    primaryWeapon[] = {"arifle_Katiba_GL_F"};
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_65x39_caseless_green_mag_Tracer",STANDARD_AMMO}};
    items[] = {{"ACRE_PRC148",1}};
    class SmokeUGL : BaseUGLSmokes {};
};

class O_Soldier_TL_F : LeaderBaseEast {
    headgear[] = {"H_HelmetSpecO_ocamo"};
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_65x39_caseless_green_mag_Tracer",STANDARD_AMMO}};
    items[] = {};
};

class O_Soldier_GL_F : BaseClassEast {
    primaryWeapon[] = {"arifle_Katiba_GL_F"};
    magazines[] = {{"30Rnd_65x39_caseless_green_mag_Tracer",STANDARD_AMMO}};
    class SmokeUGL : BaseUGLSmokes {};
    class UGL : BaseUGLs {};
};

class O_Soldier_AR_F : BaseClassEast {
    primaryWeapon[] = {"LMG_Zafir_F"};
    primaryWeaponLoadedMagazine[] = {"150Rnd_762x54_Box_Tracer"};
    binocular[] = {"Binocular"};
    magazines[] = {{"150Rnd_762x54_Box_Tracer",MG_AMMO}};
};

class O_HeavyGunner_F : BaseClassEast {
    backpack[] = {"B_Carryall_ocamo"};
    primaryWeapon[] = {"MMG_01_hex_F"};
    primaryWeaponResting[] = {"bipod_02_F_tan"};
    primaryWeaponLoadedMagazine[] = {"150Rnd_93x64_Mag"};
    magazines[] = {{"150Rnd_93x64_Mag", /*MMG_AMMO+1*/ 5}};
};

class O_Soldier_AAR_F : BaseClassEast {
    backpack[] = {"B_Carryall_ocamo"};
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_65x39_caseless_green_mag_Tracer",SPECIAL_AMMO},{"150Rnd_93x64_Mag", /*MMG_AMMO-1*/ 3},{"ACE_SpareBarrel",1}};
};

class O_soldier_M_F : BaseClassEast {
    headgear[] = {"H_HelmetSpecO_ocamo"};
    primaryWeapon[] = {"srifle_DMR_01_F"};
    primaryWeaponOptic[] = {"optic_mrco"};
    primaryWeaponLoadedMagazine[] = {"10Rnd_762x54_Mag"};
    binocular[] = {"Binocular"};
    magazines[] = {{"10Rnd_762x54_Mag", /*MRK_AMMO*2*/ 18}};
};

class O_Soldier_F : BaseClassEast {
    magazines[] = {{"30Rnd_65x39_caseless_green_mag_Tracer", RIFLEMAN_AMMO}};
};

class O_soldier_PG_F : BaseClassEast {
    primaryWeapon[] = {"arifle_Katiba_C_F"};
    primaryWeaponOptic[] = {"optic_Hamr"};
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_65x39_caseless_green_mag_Tracer",SPECIAL_AMMO},};
    items[] = {{"ACRE_PRC148",1}};
};

class CfgCLibLoadouts {

    class BaseCollection {
        uniform[] = {};
        vest[] = {};
        headgear[] = {};
        goggle[] = {};
        backpack[] = {};
        primaryWeapon[] = {};
        primaryWeaponOptic[] = {};
        primaryWeaponMuzzle[] = {};
        primaryWeaponBarrel[] = {};
        primaryWeaponResting[] = {};
        primaryWeaponLoadedMagazine[] = {};
        secondaryWeapon[] = {};
        secondaryWeaponOptic[] = {};
        secondaryWeaponMuzzle[] = {};
        secondaryWeaponBarrel[] = {};
        secondaryWeaponResting[] = {};
        secondaryWeaponLoadedMagazine[] = {};
        handgun[] = {};
        handgunOptic[] = {};
        handgunMuzzle[] = {};
        handgunBarrel[] = {};
        handgunResting[] = {};
        handgunLoadedMagazine[] = {};
        binocular[] = {};
        magazines[] = {};
        items[] = {};
        itemsUniform[] = {};
        itemsVest[] = {};
        itemsBackpack[] = {};
        linkedItems[] = {};
        script[] = {};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

	//----------------------------------------- CSAT --------------------------------------------------------//

	//---- CSAT Basisclasse
	//---- CSAT Rifleman
	
	class O_Soldier_F : BaseCollection {
	  uniform[] = {"U_O_CombatUniform_ocamo","U_O_SpecopsUniform_ocamo"};
	  vest[] = {"V_TacVest_khk"};
	  headgear[] = {"H_HelmetO_ocamo"};
	  backpack[] = {"B_FieldPack_ocamo"};
	  handgun[] = {"hgun_ACPC2_F"};
	  handgunLoadedMagazine[] = {"9Rnd_45ACP_Mag"};
	  binocular[] = {"Binocular"};
	  items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
	  linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_fadak","ItemGPS","Binocular"};
	  primaryWeapon[] = {"arifle_Katiba_F"};
	  primaryWeaponOptic[] = {"optic_ACO_grn"};
	  primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
	  magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10}};
	};


	//---- CSAT CO/XO

	class O_officer_F : O_Soldier_F {
	  headgear[] = {"H_Cap_brn_SPECOPS"};
	  backpack[] = {"tf_mr3000_multicam"};
	  items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"H_HelmetO_ocamo",1}};
	  magazines[] = {{"MiniGrenade",2},{"SmokeShell",3},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"SmokeShellGreen",2},{"SmokeShellRed",2}};

	};

	//---- CSAT Squadleader, Führungsunterstützung

	class O_Soldier_SL_F : O_Soldier_F {
		uniform[] = {"U_O_CombatUniform_ocamo","U_O_SpecopsUniform_ocamo"};
		backpack[] = {"tf_mr3000_multicam"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",3},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"SmokeShellGreen",2},{"SmokeShellRed",2}};
	};

	//---- CSAT  FTL, KPZ Kommandant, Teamleader MG
	
	class O_Soldier_TL_F : O_Soldier_F {
		primaryWeapon[] = {"arifle_Katiba_GL_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeGreen_Grenade_shell",2}};
	};

	//---- CSAT MG Schütze
	
	class O_Soldier_AR_F : O_Soldier_F {
		primaryWeapon[] = {"LMG_Mk200_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box_Tracer"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"200Rnd_65x39_cased_Box_Tracer",3}};
	};

	//---- CSAT Grenadier
	
	class O_Soldier_GL_F : O_Soldier_F {
		primaryWeapon[] = {"arifle_Katiba_GL_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",10},{"1Rnd_Smoke_Grenade_shell",5}};
	};
	
	//---- CSAT Rifleman Zusatzmaterial

	class O_Soldier_lite_F : O_Soldier_F {
		secondaryWeapon[] = {"launch_RPG32_F"};
		secondaryWeaponLoadedMagazine[] = {"RPG32_F"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",15},{"RPG32_F",1}};
		items[] = {{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_EarPlugs",1},{"ACE_morphine",5},{"ACE_epinephrine",5},{"ACE_fieldDressing",10},{"ACE_CableTie",2}};
	};

	//---- CSAT HMG Schütze
	
  class O_HeavyGunner_F : O_Soldier_F {
      primaryWeapon[] = {"MMG_02_black_F"};
      primaryWeaponResting[] = {"bipod_01_F_snd"};
      primaryWeaponLoadedMagazine[] = {"130Rnd_338_Mag"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"130Rnd_338_Mag",3}};
  };

  //---- CSAT HMG AS
  
  class O_Soldier_AAR_F : O_Soldier_F {
      backpack[] = {"B_Kitbag_rgr"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",8},{"130Rnd_338_Mag",5},{"ACE_SpareBarrel",1}};
      
  };

	//---- CSAT Scout FTL
	
	class O_recon_TL_F : O_Soldier_F {
		goggle[] = {"G_Balaclava_blk"};
		backpack[] = {"tf_mr3000_multicam"};
		primaryWeapon[] = {"arifle_Katiba_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponMuzzle[] = {"muzzle_snds_B"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green"};
		handgunMuzzle[] = {"muzzle_snds_acp"};
		binocular[] = {"Laserdesignator"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green",10}};

	};

	//---- CSAT Scout Rifleman
	
	class O_recon_F : O_Soldier_F {
		goggle[] = {"G_Balaclava_blk"};
		primaryWeapon[] = {"arifle_Katiba_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponMuzzle[] = {"muzzle_snds_B"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green"};
		handgunMuzzle[] = {"muzzle_snds_acp"};
		binocular[] = {"Laserdesignator"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green",10}};
	};

	//---- CSAT Crewman
	
	class O_crew_F : O_Soldier_F {
		headgear[] = {"H_HelmetCrew_I"};
		primaryWeapon[] = {"arifle_Katiba_C_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10}};
	};
	
	//---- CSAT Pilot
	
	class O_Pilot_F : O_Soldier_F {
		uniform[] = {"U_O_PilotCoveralls"};
		backpack[] = {""};
		vest[] = {"V_TacVest_Blk"};
		headgear[] = {"H_PilotHelmetHeli_O"};
	};


	//------------------------------------------ FIA --------------------------------------------------

	//---- IND Base Class
	//---- IND Rifleman
	
	class I_G_Soldier_F : BaseCollection {
		uniform[] = {"U_BG_Guerilla2_1","U_BG_Guerilla2_3","U_BG_Guerilla2_2","U_BG_Guerilla1_1","U_BG_Guerilla3_1","U_BG_Guerrilla_6_1","U_C_HunterBody_grn","U_I_G_resistanceLeader_F"};
		vest[] = {"V_TacVest_brn","V_TacVest_camo","V_TacVestIR_blk","V_TacVest_khk"};
		headgear[] = {"H_Bandanna_surfer","H_Bandanna_khk_hs","H_StrawHat","H_Booniehat_oli","H_Beret_blk","H_Bandanna_camo","H_Bandanna_surfer_blk","H_Bandanna_surfer_grn","H_Bandanna_sand","H_Hat_grey","H_Cap_oli"};
		backpack[] = {"B_TacticalPack_blk","B_FieldPack_oli","B_AssaultPack_rgr","B_Kitbag_sgg"};
		binocular[] = {"Binocular"};
		primaryWeapon[] = {"arifle_Mk20_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Red"};
        handgun[] = {"hgun_Rook40_F"};
	    handgunLoadedMagazine[] = {"16Rnd_9x21_Mag"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Red",10}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS"};
	};
	
	//---- IND CO/XO
	
	class I_G_officer_F : I_G_Soldier_F {
		backpack[] = {"tf_anprc155_coyote"};
	};

	//---- IND SQL, Führungsunterstützung

	class I_G_Soldier_SL_F : I_G_Soldier_F {
	    primaryWeapon[] = {"arifle_Mk20_GL_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Red"};
		backpack[] = {"tf_anprc155_coyote"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Red",10},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeYellow_Grenade_shell",2}};

	};


	//---- IND  Stv. Gruppenführer
	
	class I_G_Soldier_TL_F : I_G_Soldier_F {
		primaryWeapon[] = {"arifle_Mk20_GL_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Red"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Red",10},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeGreen_Grenade_shell",2}};
	};

	//---- IND MG Schütze
	
	class I_G_Soldier_AR_F : I_G_Soldier_F {
		primaryWeapon[] = {"LMG_Mk200_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box_Tracer"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"200Rnd_65x39_cased_Box_Tracer",3}};
	};

	//---- IND Grenadier
	
	class I_G_Soldier_GL_F : I_G_Soldier_F {
		primaryWeapon[] = {"arifle_Mk20_GL_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Red"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Red",10},{"1Rnd_HE_Grenade_shell",10},{"1Rnd_Smoke_Grenade_shell",5}};
	};

	//---- IND Schütze Zusatzmaterial

	class I_G_Soldier_lite_F : I_G_Soldier_F {
		secondaryWeapon[] = {"launch_RPG32_F"};
		secondaryWeaponLoadedMagazine[] = {"RPG32_F"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Red",10},{"RPG32_F",1}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",5},{"ACE_epinephrine",5},{"ACE_fieldDressing",10},{"ACE_CableTie",2}};
	};

	//---- IND PA Schütze
	
	class I_G_Soldier_unarmed_F : I_G_Soldier_F {
		backpack[] = {"B_Carryall_khk"};
		secondaryWeapon[] = {"launch_O_Vorona_green_F"};
		secondaryWeaponLoadedMagazine[] = {"Vorona_HEAT"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Red",8},{"Vorona_HEAT",2}};
	};

	//---- IND Aufklärer Gruppenführer
	
	class I_G_Soldier_M_F : I_G_Soldier_F {
		goggle[] = {"G_Balaclava_oli"};
		backpack[] = {"tf_mr3000_multicam"};
		primaryWeapon[] = {"arifle_Mk20_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponMuzzle[] = {"muzzle_snds_m"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Red"};
		secondaryWeaponMuzzle[] = {};
		binocular[] = {"Laserdesignator"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"ACE_30Rnd_556x45_Stanag_Tracer_Dim",10}};

	};

	//---- IND Aufklärer
	
	class I_G_Soldier_exp_F : I_G_Soldier_F {
		goggle[] = {"G_Balaclava_oli"};
		primaryWeapon[] = {"arifle_Mk20_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponMuzzle[] = {"muzzle_snds_m"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Red"};
		secondaryWeaponMuzzle[] = {"muzzle_snds_acp"};
		binocular[] = {"Laserdesignator"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"ACE_30Rnd_556x45_Stanag_Tracer_Dim",10}};
	};

	//---- IND Pilot
	
	class I_G_Survivor_F : I_G_Soldier_F {
	    primaryWeapon[] = {"arifle_Mk20C_F"};
	    primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Red"};
		uniform[] = {"U_O_PilotCoveralls"};
		vest[] = {"V_TacVest_blk"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_Tracer_Red",3},{"16Rnd_9x21_Mag",2}};
		headgear[] = {"H_PilotHelmetHeli_O"};
	};

	//---- IND Fahrer (AT Team)
	
	class I_G_Soldier_LAT_F : I_G_Soldier_F {
        backpack[] = {"tf_anprc155_coyote"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_Tracer_Red",5},{"16Rnd_9x21_Mag",2}};
	};
	
	//---- IND Schütze (AT Team)
	
	class I_G_Soldier_LAT2_F : I_G_Soldier_F {
	    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_Tracer_Red",5},{"16Rnd_9x21_Mag",2}};
	};
	
	//---- IND Fahrer (HMG Team)
	
	class I_G_engineer_F : I_G_Soldier_F {
        backpack[] = {"tf_anprc155_coyote"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_Tracer_Red",5},{"16Rnd_9x21_Mag",2}};
	};
	
	//---- IND Schütze (HMG Team)
	
	class I_G_Soldier_A_F : I_G_Soldier_F {
	    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_Tracer_Red",5},{"16Rnd_9x21_Mag",2}};
	};


	//------------------------------------------ Nato --------------------------------------------------

	//---- NATO Rifleman
	
	class B_Soldier_F : BaseCollection {
		uniform[] = {"U_B_CombatUniform_mcam_worn", "U_B_survival_uniform"};
		vest[] = {"V_PlateCarrier1_rgr"};
		headgear[] = {"H_HelmetB_sand","H_HelmetB_black","H_HelmetB_grass","H_HelmetSpecB_paint2","H_HelmetSpecB_paint1","H_HelmetSpecB","H_HelmetSpecB_blk"};
		backpack[] = {"B_AssaultPack_mcamo"};
		handgun[] = {"hgun_P07_F"};
		handgunLoadedMagazine[] = {"16Rnd_9x21_red_Mag"};
		binocular[] = {"Binocular"};
		primaryWeapon[] = {"arifle_MX_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",10}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS","Binocular","ACE_NVG_Gen4"};
		items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",2},{"ACE_epinephrine",1}};
	};

	//---- NATO Grenadier
	
	class B_Soldier_GL_F : B_Soldier_F {
		primaryWeapon[] = {"arifle_MX_GL_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",10}};
	};

	//---- NATO SQL
	
	class B_Soldier_SL_F : B_Soldier_F {
		primaryWeapon[] = {"arifle_MX_GL_F"};
		primaryWeaponOptic[] = {"optic_ACO_grn"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",10},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeGreen_Grenade_shell",2},{"1Rnd_Smoke_Grenade_shell",4}};
	};
	
	//---- NATO Pilot
	
	class B_Pilot_F : B_Soldier_F {
	    uniform[] = {"U_B_HeliPilotCoveralls"};
		headgear[] = {"H_PilotHelmetHeli_B"};
	};

	//---- NATO Technician, Doorgunner
	class B_crew_F : B_Soldier_F {
		headgear[] = {"H_CrewHelmetHeli_B"};
	    uniform[] = {"U_B_HeliPilotCoveralls"};
	};

	//------------------------------------------ CSAR --------------------------------------------------

	//---- CSAR Section Leader
	
	class B_CTRG_Soldier_TL_tna_F : BaseCollection {
		uniform[] = {"U_B_CTRG_Soldier_F"};
		vest[] = {"V_PlateCarrierH_CTRG"};
		headgear[] = {"H_HelmetB_TI_tna_F"};
		backpack[] = {"tf_rt1523g"};
		handgun[] = {"hgun_P07_F"};
		handgunLoadedMagazine[] = {"16Rnd_9x21_red_Mag"};
		handgunMuzzle[] = {"muzzle_snds_L"};
		binocular[] = {"Laserdesignator_01_khk_F"};
		primaryWeapon[] = {"arifle_SPAR_01_blk_F"};
		primaryWeaponOptic[] = {"optic_ERCO_blk_F"};
		primaryWeaponMuzzle[] = {"muzzle_snds_M"};
		primaryWeaponBarrel[] = {"acc_pointer_IR"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag"};
		magazines[] = {{"Laserbatteries",1},{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_556x45_Stanag",10}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","ItemGPS","Laserdesignator_01_khk_F"};
		items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",2},{"ACE_epinephrine",1}};
	};

	//---- CSAR Second in Command
	
	class B_Soldier_TL_F : BaseCollection {
		uniform[] = {"U_B_CTRG_Soldier_F"};
		vest[] = {"V_PlateCarrierH_CTRG"};
		goggle[] = {"G_Balaclava_TI_tna_F"};
		headgear[] = {"H_HelmetB_TI_tna_F"};
		backpack[] = {"B_AssaultPack_rgr"};
		handgun[] = {"hgun_P07_F"};
		handgunLoadedMagazine[] = {"16Rnd_9x21_red_Mag"};
		handgunMuzzle[] = {"muzzle_snds_L"};
		binocular[] = {"Laserdesignator_01_khk_F"};
		primaryWeapon[] = {"arifle_SPAR_01_blk_F"};
		primaryWeaponOptic[] = {"optic_ERCO_blk_F"};
		primaryWeaponMuzzle[] = {"muzzle_snds_M"};
		primaryWeaponBarrel[] = {"acc_pointer_IR"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag"};
		magazines[] = {{"Laserbatteries",1},{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_556x45_Stanag",10}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","ItemGPS","Laserdesignator_01_khk_F","NVGogglesB_grn_F"};
		items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",2},{"ACE_epinephrine",1}};
	};

	//---- CSAR Automatic Rifleman
	
	class B_soldier_AR_F : BaseCollection {
		uniform[] = {"U_B_CTRG_Soldier_F"};
		vest[] = {"V_PlateCarrierH_CTRG"};
		goggle[] = {"G_Balaclava_TI_tna_F"};
		headgear[] = {"H_HelmetB_TI_tna_F"};
		backpack[] = {"B_AssaultPack_rgr"};
		handgun[] = {"hgun_P07_F"};
		handgunLoadedMagazine[] = {"16Rnd_9x21_red_Mag"};
		handgunMuzzle[] = {"muzzle_snds_L"};
		binocular[] = {"Rangefinder"};
		primaryWeapon[] = {"arifle_SPAR_02_blk_F"};
		primaryWeaponOptic[] = {"optic_ERCO_blk_F"};
		primaryWeaponMuzzle[] = {"muzzle_snds_M"};
		primaryWeaponBarrel[] = {"acc_pointer_IR"};
		primaryWeaponLoadedMagazine[] = {"150Rnd_556x45_Drum_Mag_F"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"150Rnd_556x45_Drum_Mag_F",5}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","ItemGPS","Binocular","NVGogglesB_grn_F"};
		items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",2},{"ACE_epinephrine",1}};
	};
	
	//---- CSAR Grenadier
	
	class B_T_Soldier_GL_F : BaseCollection {
		uniform[] = {"U_B_CTRG_Soldier_F"};
		vest[] = {"V_PlateCarrierH_CTRG"};
		goggle[] = {"G_Balaclava_TI_tna_F"};
		headgear[] = {"H_HelmetB_TI_tna_F"};
		backpack[] = {"B_AssaultPack_rgr"};
		handgun[] = {"hgun_P07_F"};
		handgunLoadedMagazine[] = {"16Rnd_9x21_red_Mag"};
		handgunMuzzle[] = {"muzzle_snds_L"};
		binocular[] = {"Rangefinder"};
		primaryWeapon[] = {"arifle_SPAR_01_GL_blk_F"};
		primaryWeaponOptic[] = {"optic_ERCO_blk_F"};
		primaryWeaponMuzzle[] = {"muzzle_snds_M"};
		primaryWeaponBarrel[] = {"acc_pointer_IR"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_556x45_Stanag",10},{"1Rnd_HE_Grenade_shell",16},{"1Rnd_Smoke_Grenade_shell",4}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","ItemGPS","Binocular","NVGogglesB_grn_F"};
		items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",2},{"ACE_epinephrine",1}};
	};
	
	//---- CSAR Marksman
	
	class B_soldier_M_F : BaseCollection {
		uniform[] = {"U_B_CTRG_Soldier_F"};
		vest[] = {"V_PlateCarrierH_CTRG"};
		goggle[] = {"G_Balaclava_TI_tna_F"};
		headgear[] = {"H_HelmetB_TI_tna_F"};
		backpack[] = {"B_AssaultPack_rgr"};
		handgun[] = {"hgun_P07_F"};
		handgunLoadedMagazine[] = {"16Rnd_9x21_red_Mag"};
		handgunMuzzle[] = {"muzzle_snds_L"};
		binocular[] = {"Rangefinder"};
		primaryWeapon[] = {"srifle_DMR_03_F"};
		primaryWeaponOptic[] = {"optic_SOS"};
		primaryWeaponMuzzle[] = {"muzzle_snds_B"};
		primaryWeaponBarrel[] = {"acc_pointer_IR"};
		primaryWeaponResting[] = {"bipod_01_F_blk"};
		primaryWeaponLoadedMagazine[] = {"20Rnd_762x51_Mag"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"20Rnd_762x51_Mag",10}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","ItemGPS","Binocular","NVGogglesB_grn_F"};
		items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",2},{"ACE_epinephrine",1}};
	};
	
	//---- CSAR UAV-Operator
	
	class B_soldier_UAV_F : BaseCollection {
		uniform[] = {"U_B_CTRG_Soldier_F"};
		vest[] = {"V_PlateCarrierH_CTRG"};
		goggle[] = {"G_Balaclava_TI_tna_F"};
		headgear[] = {"H_HelmetB_TI_tna_F"};
		backpack[] = {"B_AssaultPack_rgr"};
		handgun[] = {"hgun_P07_F"};
		handgunLoadedMagazine[] = {"16Rnd_9x21_red_Mag"};
		handgunMuzzle[] = {"muzzle_snds_L"};
		binocular[] = {"Rangefinder"};
		primaryWeapon[] = {"arifle_SPAR_01_blk_F"};
		primaryWeaponOptic[] = {"optic_ERCO_blk_F"};
		primaryWeaponMuzzle[] = {"muzzle_snds_M"};
		primaryWeaponBarrel[] = {"acc_pointer_IR"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_556x45_Stanag",10}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","B_UavTerminal","Binocular","NVGogglesB_grn_F"};
		items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",2},{"ACE_epinephrine",1}};
	};
};

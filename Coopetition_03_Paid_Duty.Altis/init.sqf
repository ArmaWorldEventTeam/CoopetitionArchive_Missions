enableSaving [false,false];

waitUntil {!isNil "AME_Core_fnc_loadModules"};
["Core", "LoadOut", "Crates", "Environment", "Grenades", "TFAR", "GarbageCollect", "Zeus", "AWTvT", "Replay"] call AME_Core_fnc_loadModules;

call compile preprocessFileLineNumbers "fn_crateAnimation.sqf";
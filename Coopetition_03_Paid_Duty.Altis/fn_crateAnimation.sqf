JK_fnc_openTerminal = {
    ACE_player playActionNow "PutDown";
    [_this, 3] call BIS_fnc_dataTerminalAnimate;
    [{_this  setVariable ["JK_fnc_TerminalIsOpen", true, true];}, _this, 2] call ace_common_fnc_waitAndExecute
};

JK_fnc_closeTerminal = {
    ACE_player playActionNow "PutDown";
    [_this, 0] call BIS_fnc_dataTerminalAnimate;
    [{_this setVariable ["JK_fnc_TerminalIsOpen", false, true];}, _this, 2] call ace_common_fnc_waitAndExecute
};

private _action1 = ["OpenTerminal","Open Terminal","",{_target call JK_fnc_openTerminal},{!(_target getVariable ["JK_fnc_TerminalIsOpen",false])},{},[], [0,0,0], 2] call ace_interact_menu_fnc_createAction;

private _action2 = ["CloseTerminal","Close Terminal","",{_target call JK_fnc_closeTerminal},{(_target getVariable ["JK_fnc_TerminalIsOpen",false])},{},[], [0,0,0], 2] call ace_interact_menu_fnc_createAction;


[typeOf Crate1, 0, ["ACE_MainActions"],_action1] call ace_interact_menu_fnc_addActionToClass;
[typeOf Crate1, 0, ["ACE_MainActions"],_action2] call ace_interact_menu_fnc_addActionToClass;

class PistolBaseEast : BaseCollection {
    handgun[] = {"hgun_Rook40_F"};
    handgunLoadedMagazine[] = {"16Rnd_9x21_Mag"};
    magazines[] = {{"16Rnd_9x21_Mag", 1}};
};

class SignalsmokeEast : BaseCollection{
        magazines[] = {{"SmokeShellGreen",2},{"SmokeShellRed",2}};
};

class BaseClassEast : BaseCollection {
    uniform[] = {"acp_POL_wz93_core_lite_U_I_CombatUniform_POL_wz93","acp_POL_wz93_core_lite_U_I_CombatUniform_shortsleeve_POL_wz93"};
    vest[] = {"acp_Solid_CoyoteBrown_modern_east_lite_V_CF_CarrierRig_lite_Solid_CoyoteBrown"};
    headgear[] = {"acp_POL_wz93_modern_east_lite_H_HelmetSpecter_headset_POL_wz93_F","acp_POL_wz93_modern_east_lite_H_HelmetSpecter_POL_wz93_F"};

    primaryWeapon[] = {"arifle_CTAR_blk_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_F"};
    primaryWeaponBarrel[] = {"ace_acc_pointer_green"};
    primaryWeaponOptic[] = {"optic_aco"};

    magazines[] = {{"30Rnd_580x42_Mag_F",6}};

    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch"};
    items[] = {{"ACRE_PRC343",1}};
    class Pistol : PistolBaseEast {};
    class Smoke : BaseGrenades {};
    class ACEItems: BaseACEItems {};
};

class LeaderBaseEast : BaseClassEast {
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS"};
    headgear[] = {"acp_POL_wz93_modern_east_lite_H_HelmetSpecter_cover_POL_wz93_F"};
    class Smoke : BaseGrenadesLeaderBlue {};
    class LeaderItems{
        items[] = {{"ACE_microDAGR",1}};
    };
};

class PlatoonHQBaseEast: LeaderBaseEast{
};

// Factionleader
class O_officer_F : PlatoonHQBaseEast {
    binocular[] = {"Binocular"};
    class SignalSmokesItems : SignalsmokeEast{};
    class LeaderRadios{
        items[] = {{"ACRE_PRC152",2}};
    };
    magazines[] = {{"30Rnd_580x42_Mag_F",2},{"30Rnd_580x42_Mag_Tracer_F",4}};
};
// Platoon Sergant
class O_Soldier_lite_F : O_officer_F {
    class SergantExtraRadio {
        items[] = {{"ACRE_PRC152",1}};
    };

};

// Drone Operator
class O_soldier_UAV_F: PlatoonHQBaseEast{
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","O_UavTerminal"};
    backpack[] = {"O_UAV_01_backpack_F"};
    class UAVBatteries {
        items[] = {{"ACE_UAVBattery",2}};
    };
};

// PlatoonMedic
class O_soldier_repair_F: PlatoonHQBaseEast{
    backpack[] = {"acp_Solid_CoyoteBrown_core_lite_B_Kitbag_rgr_Solid_CoyoteBrown"};
    class ACEItems: ACEITemsPLMedic{};
};


// Squadleader
class O_Soldier_SL_F : LeaderBaseEast {
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_580x42_Mag_F",4},{"30Rnd_580x42_Mag_Tracer_F",2}};
    class SignalSmokesItems : SignalsmokeEast{};
    class LeaderRadios{
        items[] = {{"ACRE_PRC152",1}};
    };
};
// Teamleader
class O_Soldier_TL_F:BaseClassEast{
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS"};
    magazines[] = {{"30Rnd_580x42_Mag_F",5},{"30Rnd_580x42_Mag_Tracer_F",1}};
    class SignalSmokesItems : SignalsmokeEast{};
    binocular[] = {"Binocular"};
};

class O_Soldier_F: BaseClassEast{};

// Medic
class O_medic_F : BaseClassEast {
    backpack[] = {"acp_Solid_CoyoteBrown_core_lite_B_Kitbag_rgr_Solid_CoyoteBrown"};
    class ACEItems: ACEItemsMedic {};
    class SignalSmokesItems : SignalsmokeEast{};
};

// Automatic Rifleman
class O_soldier_AR_F : BaseClassEast {
    vest[] = {"acp_Solid_CoyoteBrown_modern_east_lite_V_CF_CarrierRig_MG_Solid_CoyoteBrown"};
    backpack[] = {"acp_POL_wz93_core_lite_B_AssaultPackEnhanced_POL_wz93"};

    primaryWeapon[] = {"arifle_CTARS_blk_F"};
    primaryWeaponLoadedMagazine[] = {"100Rnd_580x42_Mag_F"};

    magazines[] = {{"100Rnd_580x42_Mag_F", 9},{"100Rnd_580x42_Mag_Tracer_F", 2}};
};

// Grenadier
class O_Soldier_GL_F : BaseClassEast {
    backpack[] = {"acp_POL_wz93_core_lite_B_AssaultPackEnhanced_POL_wz93"};

    primaryWeapon[] = {"arifle_CTAR_GL_blk_F"};

    class SmokeUGL : BaseUGLSmokes {};
    class UGL : BaseUGLs {};
};

// Marksman
class O_soldier_M_F : BaseClassEast {

    primaryWeapon[] = {"srifle_DMR_07_blk_F"};
    primaryWeaponOptic[] = {"optic_sos"};
    primaryWeaponLoadedMagazine[] = {"20Rnd_650x39_Cased_Mag_F"};
    primaryWeaponBarrel[] = {""};

    magazines[] = {{"20Rnd_650x39_Cased_Mag_F", 10}};
    class DMItems{
        items[] = {{"ACE_RangeCard",1}};    
    };
};

// Machinegunner
class O_support_MG_F : BaseClassEast {
    backpack[] = {"acp_POL_wz93_core_lite_B_AssaultPackEnhanced_POL_wz93"};
    vest[] = {"acp_Solid_CoyoteBrown_modern_east_lite_V_CF_CarrierRig_MG_Solid_CoyoteBrown"};

    primaryWeapon[] = {"LMG_Zafir_F"};
    primaryWeaponLoadedMagazine[] = {"150Rnd_762x54_Box"};
    primaryWeaponBarrel[] = {""};
    pprimaryWeaponOptic[] = {"optic_aco"};

    magazines[] = {{"150Rnd_762x54_Box",4},{"150Rnd_762x54_Box_Tracer", 2}};
};
// ass. Machinegunner
class O_support_AMG_F : BaseClassEast {
    backpack[] = {"acp_POL_wz93_core_lite_B_AssaultPackEnhanced_POL_wz93"};
    magazines[] = {{"30Rnd_580x42_Mag_F",4},{"150Rnd_762x54_Box", 5},{"150Rnd_762x54_Box_Tracer", 2}};
    binocular[] = {"Binocular"};

};
// Teamleader CFR 
class O_engineer_F : LeaderBaseEast {
    backpack[] = {"acp_Solid_CoyoteBrown_core_lite_B_Kitbag_rgr_Solid_CoyoteBrown"};
    class ACEItems: ACEItemsMedic {};
    class LeaderRadios{
        items[] = {{"ACRE_PRC152",1}};
    };
    class SignalSmokesItems : SignalsmokeEast{};

};

// Pioniere
class O_soldier_exp_F: BaseClassEast{
    backpack[] = {"acp_POL_wz93_core_lite_B_AssaultPackEnhanced_POL_wz93"};
    class PioItems: ACEEngineerItems{};
};
class PistolBaseWest : BaseCollection {
    handgun[] = {"hgun_P07_F"};
    handgunLoadedMagazine[] = {"16Rnd_9x21_Mag"};
    magazines[] = {{"16Rnd_9x21_Mag", 1}};
};

class SignalsmokeWest : BaseCollection{
    magazines[] = {{"SmokeShellBlue",2},{"SmokeShellOrange",2}};
};

class BaseClassWest : BaseCollection {
    uniform[] = {"acp_FI_M05_Relish_core_lite_U_B_CombatUniform_FI_M05_Relish","acp_FI_M05_Relish_core_lite_U_B_CombatUniform_vest_FI_M05_Relish"};
    vest[] = {"acp_Solid_Olive_modern_west_lite_V_Crye_AVS_1_1_Solid_Olive","acp_Solid_Olive_modern_west_lite_V_Crye_AVS_1_2_Solid_Olive","acp_Solid_Olive_modern_west_lite_V_Crye_AVS_1_3_Solid_Olive"};
    headgear[] = {"acp_FI_M05_Relish_core_lite_H_HelmetSpecB_FI_M05_Relish","acp_FI_M05_Relish_core_lite_H_HelmetB_light_FI_M05_Relish","acp_FI_M05_Relish_core_lite_H_HelmetB_light_basic_FI_M05_Relish","acp_Solid_Olive_core_lite_H_HelmetB_camo_Solid_Olive"};

    primaryWeapon[] = {"arifle_SPAR_01_blk_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_red"};
    primaryWeaponBarrel[] = {"ace_acc_pointer_green"};
    primaryWeaponOptic[] = {"optic_holosight_blk_f"};

    magazines[] = {{"30Rnd_556x45_Stanag_red",6}};

    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch"};
    items[] = {{"ACRE_PRC343",1}};
    class Pistol : PistolBaseWest {};
    class Smoke : BaseGrenades {};
    class ACEItems: BaseACEItems {};
};

class LeaderBaseWest : BaseClassWest {
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS"};
    class Smoke : BaseGrenadesLeaderBlue {};
    class LeaderItems{
         items[] = {{"ACE_microDAGR",1}};
    };
};

class PlatoonHQBaseWest: LeaderBaseWest{
    headgear[] = {"acp_Solid_Olive_core_lite_H_HelmetSpecB_Solid_Olive","acp_Solid_Olive_core_lite_H_HelmetB_light_basic_Solid_Olive","acp_Solid_Olive_core_lite_H_HelmetB_light_Solid_Olive"};
};

// Factionleader
class B_officer_F : PlatoonHQBaseWest {
    binocular[] = {"Binocular"};
    class SignalSmokesItems : SignalsmokeWest{};
    class LeaderRadios{
        items[] = {{"ACRE_PRC152",2}};
    };
    magazines[] = {{"30Rnd_556x45_Stanag_red",2},{"30Rnd_556x45_Stanag_Tracer_Red",4}};
};
// Platoon Sergant
class B_Soldier_lite_F : B_officer_F {
    class SergantExtraRadio {
        items[] = {{"ACRE_PRC152",1}};
    };
};

// Drone Operator
class B_soldier_UAV_F: PlatoonHQBaseWest{
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","B_UavTerminal"};
    backpack[] = {"B_UAV_01_backpack_F"};
    class UAVBatteries {
        items[] = {{"ACE_UAVBattery",2}};
    };
};

// PlatoonMedic
class B_soldier_repair_F: PlatoonHQBaseWest{
    backpack[] = {"acp_Solid_Olive_core_lite_B_Kitbag_rgr_Solid_Olive"};
    class ACEItems: ACEITemsPLMedic{};
};


// Squadleader
class B_Soldier_SL_F : LeaderBaseWest {
    binocular[] = {"Binocular"};
    magazines[] = {{"30Rnd_556x45_Stanag_red",4},{"30Rnd_556x45_Stanag_Tracer_Red",2}};
    class SignalSmokesItems : SignalsmokeWest{};
    class LeaderRadios{
        items[] = {{"ACRE_PRC152",1}};
    };
};

// Teamleader
class B_Soldier_TL_F: BaseClassWest {
    magazines[] = {{"30Rnd_556x45_Stanag_red",5},{"30Rnd_556x45_Stanag_Tracer_Red",1}};
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS"};
    class SignalSmokesItems : SignalsmokeWest{};
    binocular[] = {"Binocular"};

};

class B_Soldier_F: BaseClassWest{};

// Medic
class B_medic_F : BaseClassWest {
    backpack[] = {"acp_Solid_Olive_core_lite_B_Kitbag_rgr_Solid_Olive"};
    class ACEItems: ACEItemsMedic {};
    class SignalSmokesItems : SignalsmokeWest{};
};

// Automatic Rifleman
class B_soldier_AR_F : BaseClassWest {
    vest[] = {"acp_Solid_Olive_modern_west_lite_V_Crye_AVS_2_Solid_Olive"};
    backpack[] = {"acp_Solid_Olive_core_lite_B_AssaultPackEnhanced_Solid_Olive"};

    primaryWeapon[] = {"arifle_SPAR_02_blk_F"};
    primaryWeaponLoadedMagazine[] = {"150Rnd_556x45_Drum_Mag_F"};
    magazines[] = {{"150Rnd_556x45_Drum_Mag_F", 5},{"150Rnd_556x45_Drum_Mag_Tracer_F", 2}};
};

// Grenadier
class B_Soldier_GL_F : BaseClassWest {
    backpack[] = {"acp_Solid_Olive_core_lite_B_AssaultPackEnhanced_Solid_Olive"};
    primaryWeapon[] = {"arifle_SPAR_01_GL_blk_F"};
    class SmokeUGL : BaseUGLSmokes {};
    class UGL : BaseUGLs {};
};

// Marksman
class B_soldier_M_F : BaseClassWest {
    vest[] = {"acp_Solid_Olive_modern_west_lite_V_Crye_AVS_1_Solid_Olive"};
    primaryWeapon[] = {"srifle_EBR_F"};
    primaryWeaponOptic[] = {"optic_sos"};
    primaryWeaponLoadedMagazine[] = {"20Rnd_762x51_Mag"};
    primaryWeaponBarrel[] = {"ace_acc_pointer_green"};

    magazines[] = {{"20Rnd_762x51_Mag", 7},{"ACE_20Rnd_762x51_Mag_Tracer", 3}};
    class DMItems{
        items[] = {{"ACE_RangeCard",1}};    
    };
};

// Machinegunner
class B_support_MG_F : BaseClassWest {
    backpack[] = {"acp_Solid_Olive_core_lite_B_AssaultPackEnhanced_Solid_Olive"};
    vest[] = {"acp_Solid_Olive_modern_west_lite_V_Crye_AVS_2_Solid_Olive"};

    primaryWeapon[] = {"LMG_Mk200_F"};
    primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box_Red"};
    primaryWeaponBarrel[] = {"ace_acc_pointer_green"};
    primaryWeaponOptic[] = {"optic_holosight_blk_f"};

    magazines[] = {{"200Rnd_65x39_cased_Box_Red", 3},{"200Rnd_65x39_cased_Box_Tracer_Red", 2}};
};

// ass. Machinegunner
class B_support_AMG_F : BaseClassWest {
    backpack[] = {"acp_Solid_Olive_core_lite_B_AssaultPackEnhanced_Solid_Olive"};
    magazines[] = {{"30Rnd_556x45_Stanag_red",4},{"200Rnd_65x39_cased_Box_Red", 3},{"200Rnd_65x39_cased_Box_Tracer_Red", 2}};
    binocular[] = {"Binocular"};
};

// Teamleader CFR 
class B_engineer_F : LeaderBaseWest {
    backpack[] = {"acp_Solid_Olive_core_lite_B_Kitbag_rgr_Solid_Olive"};
    class ACEItems: ACEItemsMedic {};
    class LeaderRadios{
        items[] = {{"ACRE_PRC152",1}};
    };
    class SignalSmokesItems : SignalsmokeWest{};
};

// Pioniere
class B_soldier_exp_F: BaseClassWest{
    backpack[] = {"acp_Solid_Olive_core_lite_B_AssaultPackEnhanced_Solid_Olive"};   
    class PioItems: ACEEngineerItems{};
};
enableSaving [false, false];

if (!isNil "CLib_fnc_loadModules") then {
	call CLib_fnc_loadModules;
} else {
	diag_log "[ERROR]: Running init.sqf but the CLib functions are not present!";
};

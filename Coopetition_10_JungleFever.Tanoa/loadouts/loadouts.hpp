class CfgCLibLoadouts {
  class BaseCollection {
      uniform[] = {};
      vest[] = {};
      headgear[] = {};
      goggle[] = {};
      backpack[] = {};
      primaryWeapon[] = {};
      primaryWeaponOptic[] = {};
      primaryWeaponMuzzle[] = {};
      primaryWeaponBarrel[] = {};
      primaryWeaponResting[] = {};
      primaryWeaponLoadedMagazine[] = {};
      secondaryWeapon[] = {};
      secondaryWeaponOptic[] = {};
      secondaryWeaponMuzzle[] = {};
      secondaryWeaponBarrel[] = {};
      secondaryWeaponResting[] = {};
      secondaryWeaponLoadedMagazine[] = {};
      handgun[] = {};
      handgunOptic[] = {};
      handgunMuzzle[] = {};
      handgunBarrel[] = {};
      handgunResting[] = {};
      handgunLoadedMagazine[] = {};
      binocular[] = {};
      magazines[] = {};
      items[] = {};
      itemsUniform[] = {};
      itemsVest[] = {};
      itemsBackpack[] = {};
      linkedItems[] = {};
      script[] = {};
      removeAllWeapons = 1;
      removeAllItems = 1;
      removeAllAssignedItems = 1;
  };

  // NATO Basis ------------------------------------------------------------------------------------------------------
  class nato_baseclass : BaseCollection {
      uniform[] = {"U_B_T_Soldier_F","U_B_T_Soldier_AR_F"};
      vest[] = {"V_PlateCarrier2_tna_F","V_PlateCarrierSpec_rgr"};
      headgear[] = {"H_HelmetB_tna_F"};
      backpack[] = {"B_AssaultPack_tna_F"};
      handgun[] = {"hgun_P07_khk_F"};
      handgunLoadedMagazine[] = {"16Rnd_9x21_Mag"};
      binocular[] = {"Binocular"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
      linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS","Binocular"};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_Flashlight_XL50",1}};

  };

  // Commander
  class B_officer_F : nato_baseclass {
      headgear[] = {"H_Beret_02"};
      backpack[] = {"B_Kitbag_rgr"};
      primaryWeapon[] = {"arifle_MX_khk_F"};
      primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
      primaryWeaponBarrel[] = {"acc_flashlight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_khaki_mag_Tracer"};
      itemsBackpack[] = {{"ACRE_PRC117F",1}};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",6},{"SmokeShellGreen",1}};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1}};
  };

  //SQl
class B_Soldier_SL_F : nato_baseclass {
    backpack[] = {"B_Kitbag_rgr"};
    primaryWeapon[] = {"arifle_MX_khk_F"};
    primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
    primaryWeaponBarrel[] = {"acc_flashlight"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_khaki_mag_Tracer"};
    itemsBackpack[] = {{"ACRE_PRC117F",1}};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",6},{"SmokeShellGreen",1}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1}};
};

// Teamleader
class B_Soldier_TL_F : nato_baseclass {
    primaryWeapon[] = {"arifle_MX_khk_F"};
    primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
    primaryWeaponBarrel[] = {"acc_flashlight"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_khaki_mag_Tracer"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",10},{"SmokeShellGreen",1}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACRE_PRC148",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1}};
};

  // Grenadier
  class B_Soldier_GL_F : nato_baseclass {
      primaryWeapon[] = {"arifle_MX_GL_khk_F"};
      primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
      primaryWeaponBarrel[] = {"acc_flashlight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_khaki_mag_Tracer"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",5}};
  };

  // Rifleman
  class B_Soldier_F	: nato_baseclass {
      primaryWeapon[] = {"arifle_MX_khk_F"};
      primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
      primaryWeaponBarrel[] = {"acc_flashlight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_khaki_mag_Tracer"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",14}};
  };

  // Autorifleman
  class B_soldier_AR_F	: nato_baseclass {
      primaryWeapon[] = {"LMG_03_F"};
      primaryWeaponBarrel[] = {"acc_flashlight"};
      primaryWeaponLoadedMagazine[] = {"200Rnd_556x45_Box_Tracer_F"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"200Rnd_556x45_Box_Tracer_F",5}};
  };

  // Schütze AT (Rifelman)
  class B_soldier_LAT_F	: nato_baseclass {
      primaryWeapon[] = {"arifle_MX_khk_F"};
      primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
      primaryWeaponBarrel[] = {"acc_flashlight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_khaki_mag_Tracer"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",14}};
  };

  // recon Teamleader
  class B_recon_TL_F : nato_baseclass {
      uniform[] = {"U_B_T_FullGhillie_tna_F"};
      goggle[] = {"G_Bandanna_oli"};
      backpack[] = {"B_Kitbag_rgr"};
      primaryWeapon[] = {"arifle_MXM_khk_F"};
      primaryWeaponOptic[] = {"optic_SOS_khk_F"};
      primaryWeaponBarrel[] = {"acc_flashlight"};
      primaryWeaponMuzzle[] = {"ACE_muzzle_mzls_H"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_khaki_mag"};
      binocular[] = {"ACE_Vector"};
      itemsBackpack[] = {{"ACRE_PRC117F",1}};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_khaki_mag",6},{"SmokeShellGreen",1}};
      linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS","ACE_Vector"};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1},{"ACE_RangeCard",1},{"ACE_Kestrel4500",1}};
  };

  // recon marksman
  class B_recon_M_F : nato_baseclass {
      uniform[] = {"U_B_T_FullGhillie_tna_F"};
      goggle[] = {"G_Bandanna_oli"};
      primaryWeapon[] = {"arifle_MXM_khk_F"};
      primaryWeaponOptic[] = {"optic_SOS_khk_F"};
      primaryWeaponBarrel[] = {"acc_flashlight"};
      primaryWeaponMuzzle[] = {"ACE_muzzle_mzls_H"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_khaki_mag"};
      binocular[] = {"ACE_Vector"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_khaki_mag",6},{"SmokeShellGreen",1}};
      linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS","ACE_Vector"};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1},{"ACE_RangeCard",1},{"ACE_Kestrel4500",1}};
  };

    // CSAT Basis ------------------------------------------------------------------------------------------------------
    class csat_baseclass : BaseCollection {
        uniform[] = {"U_O_T_Soldier_F"};
        vest[] = {"V_HarnessO_ghex_F"};
        headgear[] = {"H_HelmetO_ghex_F"};
        backpack[] = {"B_FieldPack_ghex_F"};
        handgun[] = {"hgun_Rook40_F"};
        handgunLoadedMagazine[] = {"16Rnd_9x21_Mag"};
        binocular[] = {"Binocular"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS","Binocular"};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_Flashlight_XL50",1}};

    };

    // Commander
    class O_officer_F : csat_baseclass {
        headgear[] = {"H_Beret_CSAT_01_F"};
        backpack[] = {"B_Carryall_ghex_F"};
        primaryWeapon[] = {"arifle_CTAR_ghex_F"};
        primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
        primaryWeaponBarrel[] = {"acc_flashlight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_Tracer_F"};
        itemsBackpack[] = {{"ACRE_PRC117F",1}};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_580x42_Mag_Tracer_F",6},{"SmokeShellGreen",1}};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1}};
    };

    //SQl
  class O_Soldier_SL_F : csat_baseclass {
      backpack[] = {"B_Carryall_ghex_F"};
      primaryWeapon[] = {"arifle_CTAR_ghex_F"};
      primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
      primaryWeaponBarrel[] = {"acc_flashlight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_Tracer_F"};
      itemsBackpack[] = {{"ACRE_PRC117F",1}};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_580x42_Mag_Tracer_F",6},{"SmokeShellGreen",1}};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1}};
  };

  // Teamleader
  class O_Soldier_TL_F : csat_baseclass {
      primaryWeapon[] = {"arifle_CTAR_ghex_F"};
      primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
      primaryWeaponBarrel[] = {"acc_flashlight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_Tracer_F"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_580x42_Mag_Tracer_F",10},{"SmokeShellGreen",1}};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACRE_PRC148",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1}};
  };

    // Grenadier
    class O_Soldier_GL_F : csat_baseclass {
        primaryWeapon[] = {"arifle_CTAR_GL_ghex_F"};
        primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
        primaryWeaponBarrel[] = {"acc_flashlight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_Tracer_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_580x42_Mag_Tracer_F",10},{"1Rnd_HE_Grenade_shell",5}};
    };

    // Rifleman
    class O_Soldier_F	: csat_baseclass {
        primaryWeapon[] = {"arifle_CTAR_ghex_F"};
        primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
        primaryWeaponBarrel[] = {"acc_flashlight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_Tracer_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_580x42_Mag_Tracer_F",14}};
    };

    // Autorifleman
    class O_Soldier_AR_F	: csat_baseclass {
        primaryWeapon[] = {"LMG_Mk200_F"};
        primaryWeaponBarrel[] = {"acc_flashlight"};
        primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"200Rnd_65x39_cased_Box_Tracer",5}};
    };

    // Schütze AT (rifelman)
    class O_Soldier_LAT_F	: csat_baseclass {
        primaryWeapon[] = {"arifle_CTAR_ghex_F"};
        primaryWeaponOptic[] = {"optic_Holosight_khk_F"};
        primaryWeaponBarrel[] = {"acc_flashlight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_Tracer_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_580x42_Mag_Tracer_F",14}};
    };

    // recon Teamleader
    class O_recon_TL_F : csat_baseclass {
        uniform[] = {"U_O_T_FullGhillie_tna_F"};
        goggle[] = {"G_Bandanna_oli"};
        backpack[] = {"B_Carryall_ghex_F"};
        primaryWeapon[] = {"srifle_DMR_01_F"};
        primaryWeaponOptic[] = {"optic_SOS"};
        primaryWeaponBarrel[] = {"acc_flashlight"};
        primaryWeaponMuzzle[] = {"muzzle_snds_B"};
        primaryWeaponLoadedMagazine[] = {"10Rnd_762x54_Mag"};
        binocular[] = {"ACE_Vector"};
        itemsBackpack[] = {{"ACRE_PRC117F",1}};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"10Rnd_762x54_Mag",6},{"SmokeShellGreen",1}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS","ACE_Vector"};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1},{"ACE_RangeCard",1},{"ACE_Kestrel4500",1}};
    };

    // recon marksman
    class O_recon_M_F : csat_baseclass {
        uniform[] = {"U_O_T_FullGhillie_tna_F"};
        goggle[] = {"G_Bandanna_oli"};
        primaryWeapon[] = {"srifle_DMR_01_F"};
        primaryWeaponOptic[] = {"optic_SOS"};
        primaryWeaponBarrel[] = {"acc_flashlight"};
        primaryWeaponMuzzle[] = {"muzzle_snds_B"};
        primaryWeaponLoadedMagazine[] = {"10Rnd_762x54_Mag"};
        binocular[] = {"ACE_Vector"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"10Rnd_762x54_Mag",6},{"SmokeShellGreen",1}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS","ACE_Vector"};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1},{"ACE_RangeCard",1},{"ACE_Kestrel4500",1}};
    };

    // greenfor Basis ------------------------------------------------------------------------------------------------------
    class syndicat_baseclass : BaseCollection {
        uniform[] = {"U_I_C_Soldier_Para_2_F","U_I_C_Soldier_Para_4_F","U_I_C_Soldier_Para_3_F","U_I_C_Soldier_Para_1_F","U_I_C_Soldier_Camo_F"};
        vest[] = {"V_TacVest_oli","V_I_G_resistanceLeader_F","V_TacVest_blk"};
        headgear[] = {"H_Booniehat_tna_F"};
        goggle[] = {"G_Balaclava_blk"};
        handgun[] = {"hgun_P07_blk_F"};
        handgunLoadedMagazine[] = {"16Rnd_9x21_Mag"};
        binocular[] = {"Binocular"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS","Binocular"};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",1},{"ACE_Flashlight_XL50",1}};

    };

    // Commander
    class I_Soldier_SL_F : syndicat_baseclass {
        headgear[] = {"H_Bandanna_camo"};
        primaryWeapon[] = {"arifle_AK12_F"};
        primaryWeaponBarrel[] = {"acc_flashlight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_762x39_AK12_Mag_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_762x39_AK12_Mag_F",2},{"SmokeShellGreen",1}};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACRE_PRC152",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1}};
    };

    // Teamleader
    class I_Soldier_TL_F : syndicat_baseclass {
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"SmokeShellGreen",1}};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACRE_PRC152",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_Flashlight_XL50",1}};
    };

    // Rifleman
    class I_soldier_F	: syndicat_baseclass {
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_9x21_Mag_SMG_02",4}};
    };

    //  Zeus

    class C_Soldier_VR_F : BaseCollection {
        backpack[] = {"B_Carryall_ghex_F"};
        itemsBackpack[] = {{"ACRE_PRC117F",1}};
    };
};

class CfgCLibLoadouts {

    class BaseCollection {
        uniform[] = {};
        vest[] = {};
        headgear[] = {};
        goggle[] = {};
        backpack[] = {};
        primaryWeapon[] = {};
        primaryWeaponOptic[] = {};
        primaryWeaponMuzzle[] = {};
        primaryWeaponBarrel[] = {};
        primaryWeaponResting[] = {};
        primaryWeaponLoadedMagazine[] = {};
        secondaryWeapon[] = {};
        secondaryWeaponOptic[] = {};
        secondaryWeaponMuzzle[] = {};
        secondaryWeaponBarrel[] = {};
        secondaryWeaponResting[] = {};
        secondaryWeaponLoadedMagazine[] = {};
        handgun[] = {};
        handgunOptic[] = {};
        handgunMuzzle[] = {};
        handgunBarrel[] = {};
        handgunResting[] = {};
        handgunLoadedMagazine[] = {};
        binocular[] = {};
        magazines[] = {};
        items[] = {};
        itemsUniform[] = {};
        itemsVest[] = {};
        itemsBackpack[] = {};
        linkedItems[] = {};
        script[] = {};
        removeAllWeapons = 0;
        removeAllItems = 0;
        removeAllAssignedItems = 0;
    };

//PCT Grundausrüstung ----------------------------------------------------------------------------------------------------------------------------------
	class gre_baseclass : BaseCollection {
        uniform[] = {"U_BG_Guerilla2_1","U_BG_Guerilla2_3","U_BG_Guerilla2_2","U_BG_Guerilla1_1","U_BG_Guerilla3_1","U_BG_Guerrilla_6_1","U_C_HunterBody_grn"};
        vest[] = {"V_I_G_resistanceLeader_F","V_TacVest_camo","V_TacVestIR_blk","V_PlateCarrier2_blk","V_PlateCarrier1_blk","V_PlateCarrier1_rgr_noflag_F"};
        headgear[] = {"H_Bandanna_surfer","H_Bandanna_khk_hs","H_StrawHat","H_Booniehat_oli","H_Beret_blk","H_Bandanna_camo","H_Bandanna_surfer_blk","H_Bandanna_surfer_grn","H_Bandanna_sand","H_Hat_grey","H_Cap_oli"};
        goggle[] = {"","G_Bandanna_beast","G_Bandanna_blk","G_Bandanna_khk","G_Bandanna_shades","G_Balaclava_blk",""};
        backpack[] = {"B_TacticalPack_blk","B_FieldPack_oli","B_AssaultPack_rgr","B_Kitbag_sgg"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_580x42_Mag_Tracer_F",10}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem"};
        primaryWeapon[] = {"arifle_CTAR_blk_F"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_Tracer_F"};
		removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

// PCT Commanding/ Executive Officer
	class O_officer_F : gre_baseclass {
        backpack[] = {"tf_anprc155"};
		binocular[] = {"Binocular"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_Tracer_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_580x42_Mag_Tracer_F",5}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS"};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2},{"ACE_MapTools",1}};
		secondaryWeapon[] = {"hgun_Rook40_F"};
		secondaryWeaponLoadedMagazine[] = {"16Rnd_9x21_Mag"};
    };

// PCT Squadleader
	class O_Soldier_SL_F : gre_baseclass {
        backpack[] = {"tf_anprc155"};
		binocular[] = {"Binocular"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_580x42_Mag_Tracer_F",10}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS",};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2},{"ACE_MapTools",1}};
    };

// PCT Fireteamleader
	class O_Soldier_TL_F : gre_baseclass {
		binocular[] = {"Binocular"};
    };

// PCT Automatic Rifleman
	class O_Soldier_AR_F : gre_baseclass {
        primaryWeapon[] = {"arifle_CTARS_blk_F"};
		primaryWeaponLoadedMagazine[] = {"100Rnd_580x42_Mag_Tracer_F"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"100Rnd_580x42_Mag_Tracer_F",7}};
    };

// PCT Grenadier
	class O_Soldier_GL_F : gre_baseclass {
        primaryWeapon[] = {"arifle_CTAR_GL_blk_F"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_Tracer_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_580x42_Mag_Tracer_F",10},{"1Rnd_HE_Grenade_shell",8}};
    };

// PCT Rifleman 
	class O_Soldier_F : gre_baseclass {
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",6},{"100Rnd_580x42_Mag_Tracer_F",6},{"30Rnd_580x42_Mag_Tracer_F",18}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",6},{"ACE_fieldDressing",20},{"ACE_CableTie",2},{"ACE_EntrenchingTool",1}};
    };

// PCT MG-Teamleader
	class O_support_GMG_F : gre_baseclass {
		backpack[] = {"tf_anprc155"};
		binocular[] = {"Binocular"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_580x42_Mag_Tracer_F",10},{"130Rnd_338_Mag",3}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS",};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2},{"ACE_MapTools",1}};
	};

// PCT MMG1
	class O_support_MG_F : gre_baseclass {
        primaryWeapon[] = {"MMG_02_black_F"};
        primaryWeaponLoadedMagazine[] = {"130Rnd_338_Mag"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"130Rnd_338_Mag",4}};
		secondaryWeapon[] = {"hgun_Rook40_F"};
		secondaryWeaponLoadedMagazine[] = {"16Rnd_9x21_Mag"};
    };

// PCT MMG2
	class O_Soldier_A_F : gre_baseclass {
		binocular[] = {"Binocular"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"130Rnd_338_Mag",5},{"30Rnd_580x42_Mag_Tracer_F",7}};
    };

// PCT Recon Teamleader
	class O_soldier_UAV_F : gre_baseclass {
		backpack[] = {"tf_anprc155"};
		binocular[] = {"Binocular"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_580x42_Mag_F",6}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2},{"ACE_MapTools",1}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_580x42_Mag_F"};
    };

// PCT Recon
	class O_soldier_M_F : gre_baseclass {
        primaryWeapon[] = {"srifle_DMR_07_blk_F"};
		primaryWeaponOptic[] = {"ace_optic_mrco_2d"};
        primaryWeaponLoadedMagazine[] = {"20Rnd_650x39_Cased_Mag_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"20Rnd_650x39_Cased_Mag_F",6}};
		secondaryWeapon[] = {"hgun_Rook40_F"};
		primaryWeaponLoadedMagazine[] = {"16Rnd_9x21_Mag"};
		backpack[] = {"O_Static_Designator_02_weapon_F"};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","O_UavTerminal"};
    };

//NATO Grundausrüstung ----------------------------------------------------------------------------------------------------------------------------------
	class nato_baseclass : BaseCollection {
		uniform[] = {"U_B_CombatUniform_mcam","U_B_CombatUniform_mcam_tshirt"};
		vest[] = {"V_PlateCarrier2_rgr","V_PlateCarrier2_rgr"};
		headgear[] = {"H_HelmetB","H_HelmetB_tna_F","H_HelmetB_Enh_tna_F"};  
		backpack[] = {"B_AssaultPack_mcamo","B_Kitbag_mcamo"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_Tracer_Red",10}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","TFAR_anprc154"};
		primaryWeapon[] = {"arifle_SPAR_01_khk_F"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Red"};
		removeAllWeapons = 1;
		removeAllItems = 1;
		removeAllAssignedItems = 1;
	};

// NATO Commanding/ Executive Officer/ Plt. Leader/ Sgt.
	class B_officer_F : nato_baseclass {
		uniform[] = {"U_B_CombatUniform_mcam"};
		backpack[] = {"TFAR_rt1523g_green"};
		binocular[] = {"Binocular"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Red",5}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2},{"ACE_MapTools",1}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","TFAR_anprc154","ItemGPS"};
		secondaryWeapon[] = {"hgun_P07_F"};
		secondaryWeaponLoadedMagazine[] = {"16Rnd_9x21_Mag"};
	};

// NATO Squadleader
	class B_Soldier_SL_F : nato_baseclass {
		backpack[] = {"TFAR_rt1523g_green"};
		binocular[] = {"Binocular"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_Tracer_Red",10}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2},{"ACE_MapTools",1}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","TFAR_anprc154","ItemGPS"};
	};

// NATO Fireteamleader
	class B_Soldier_TL_F : nato_baseclass {
		binocular[] = {"Binocular"};
	};

// NATO Automatic Rifleman
	class B_soldier_AR_F : nato_baseclass {
		primaryWeapon[] = {"arifle_SPAR_02_khk_F"};
		primaryWeaponLoadedMagazine[] = {"150Rnd_556x45_Drum_Green_Mag_Tracer_F"};
		primaryWeaponResting[] = {"bipod_01_f_blk"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"150Rnd_556x45_Drum_Green_Mag_Tracer_F",5}};
	};

// NATO Grenadier
	class B_Soldier_GL_F : nato_baseclass {
		primaryWeapon[] = {"arifle_SPAR_01_GL_khk_F"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Red"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_Tracer_Red",10},{"1Rnd_HE_Grenade_shell",8}};
	};

// NATO Rifleman 
	class B_Soldier_F : nato_baseclass {
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",6},{"150Rnd_556x45_Drum_Green_Mag_Tracer_F",2},{"30Rnd_556x45_Stanag_Tracer_Red",20}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",6},{"ACE_fieldDressing",20},{"ACE_CableTie",5},{"ACE_EntrenchingTool",1}};
	};

// NATO Gendarmerie Squad Leader
	class B_G_Soldier_SL_F : nato_baseclass {
		uniform[] = {"U_B_GEN_Soldier_F"};
		vest[] = {"V_TacVest_gen_F"};
		backpack[] = {"TFAR_rt1523g_green"};
		binocular[] = {"Binocular"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_Tracer_Red",10}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2},{"ACE_MapTools",1}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","TFAR_anprc154","ItemGPS"};
	};

// NATO Gendarmerie Fireteamleader
	class B_G_Soldier_TL_F : nato_baseclass {
		uniform[] = {"U_B_GEN_Soldier_F"};
		vest[] = {"V_TacVest_gen_F"};
		binocular[] = {"Binocular"};
	};

// NATO Gendarmerie Automatic Rifleman
	class B_G_Soldier_AR_F : nato_baseclass {
		uniform[] = {"U_B_GEN_Soldier_F"};
		vest[] = {"V_TacVest_gen_F"};
		primaryWeapon[] = {"arifle_SPAR_02_khk_F"};
		primaryWeaponLoadedMagazine[] = {"150Rnd_556x45_Drum_Green_Mag_Tracer_F"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"150Rnd_556x45_Drum_Green_Mag_Tracer_F",5}};
	};

// NATO Gendarmerie Grenadier
	class B_G_Soldier_GL_F : nato_baseclass {
		uniform[] = {"U_B_GEN_Soldier_F"};
		vest[] = {"V_TacVest_gen_F"};
		primaryWeapon[] = {"arifle_SPAR_01_GL_khk_F"};
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Red"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_Tracer_Red",10},{"1Rnd_HE_Grenade_shell",8}};
	};

// NATO Gendarmerie Rifleman 
	class B_G_Soldier_F : nato_baseclass {
		uniform[] = {"U_B_GEN_Soldier_F"};
		vest[] = {"V_TacVest_gen_F"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",6},{"150Rnd_556x45_Drum_Green_Mag_Tracer_F",2},{"30Rnd_556x45_Stanag_Tracer_Red",20}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",6},{"ACE_fieldDressing",20},{"ACE_CableTie",5},{"ACE_EntrenchingTool",1}};
	};

// NATO MMG1
	class B_support_MG_F	 : nato_baseclass {
		primaryWeapon[] = {"MMG_01_tan_F"};
		primaryWeaponLoadedMagazine[] = {"150Rnd_93x64_Mag"};
		secondaryWeapon[] = {"hgun_P07_F"};
		secondaryWeaponLoadedMagazine[] = {"16Rnd_9x21_Mag"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"150Rnd_93x64_Mag",4},{"16Rnd_9x21_Mag",2}};
	};

// NATO MMG2
	class B_Soldier_A_F : nato_baseclass {
		binocular[] = {"Binocular"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"150Rnd_93x64_Mag",3},{"30Rnd_556x45_Stanag_Tracer_Red",7}};
	};

// NATO Recon Teamleader
	class B_soldier_UAV_F : nato_baseclass {
		primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_red"};
		backpack[] = {"TFAR_rt1523g_green"};
		binocular[] = {"Laserdesignator_03"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_556x45_Stanag_red",6}};
		items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2},{"ACE_MapTools",1}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","TFAR_anprc154","ItemGPS",};
	};

// NATO Recon
	class B_soldier_M_F : nato_baseclass {
		primaryWeapon[] = {"srifle_DMR_06_camo_F"};
		primaryWeaponOptic[] = {"ace_optic_mrco_2d"};
		primaryWeaponLoadedMagazine[] = {"20Rnd_762x51_Mag"};
        primaryWeaponResting[] = {"bipod_01_f_blk"};
		binocular[] = {"Laserdesignator_03"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"20Rnd_762x51_Mag",6}};
		secondaryWeapon[] = {"hgun_P07_F"};
		secondaryWeaponLoadedMagazine[] = {"16Rnd_9x21_Mag"};
	};
};
class PistolBaseEast : BaseCollection {
    handgun[] = {"hgun_Rook40_F"};
    handgunLoadedMagazine[] = {"16Rnd_9x21_Mag"};
    magazines[] = {{"16Rnd_9x21_Mag", 1}};
};

class BaseClassEast : BaseCollection {
    uniform[] = {"acp_CSAT_GHex_U_BG_Guerrilla_6_CSAT_GHex_insignia"};
    vest[] = {"acp_CSAT_GHex_V_CarrierRigBW_Lite_CSAT_GHex"};
    headgear[] = {"acp_CSAT_GHex_H_HelmetIA_CSAT_GHex"};

    primaryWeapon[] = {"arifle_Katiba_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green"};
    primaryWeaponBarrel[] = {"acc_omnilight"};
    primaryWeaponOptic[] = {"optic_aco_grn"};

    magazines[] = {{"30Rnd_65x39_caseless_green",5},{"30Rnd_65x39_caseless_green_mag_Tracer",2}};

    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch"};

    class Pistol : PistolBaseEastt {};
    class Smoke : BaseGrenades {};
    class ACEItems: BaseACEItems {};
};

class LeaderBaseEast : BaseClassEast {
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS"};
    class Smoke : BaseGrenadesLeader {};
};
// Factionleader
class O_officer_F : LeaderBaseEast {
    items[] = {{"ACE_microDAGR",1},{"ACRE_PRC148",1}};
};

// Squadleader
class O_Soldier_SL_F : O_officer_F {
    backpack[] = {"acp_CSAT_GHex_B_AssaultPackEnhanced_CSAT_GHex_insignia"};
    items[] = {{"ACE_microDAGR",1},{"ACRE_PRC148",2}};
};

// Teamleader
class O_Soldier_TL_F : O_Soldier_SL_F {
    items[] = {{"ACE_microDAGR",1},{"ACRE_PRC148",1}};
};


// Radioman
class O_crew_F : LeaderBaseEast {
    backpack[] = {"acp_CSAT_GHex_B_Kitbag_rgr_CSAT_GHex"};
    items[] = {{"ACE_microDAGR",1},{"ACRE_PRC148",1},{"ACRE_PRC77",1}};
};

// Medic
class O_medic_F : BaseClassEast {
    backpack[] = {"acp_CSAT_GHex_B_Kitbag_rgr_CSAT_GHex"};
    class ACEItems : ACEItemsMedic {};
};

// Automatic Rifleman
class O_Soldier_AR_F : BaseClassEast {
    backpack[] = {"acp_CSAT_GHex_B_AssaultPackEnhanced_CSAT_GHex_insignia"};

    primaryWeapon[] = {"LMG_Zafir_F"};
    primaryWeaponOptic[] = {"optic_holosight"};
    primaryWeaponLoadedMagazine[] = {"150Rnd_762x54_Box_Tracer"};
    magazines[] = {{"150Rnd_762x54_Box", 4},{"150Rnd_762x54_Box_Tracer",2}};
};

// Grenadier
class O_Soldier_GL_F : BaseClassEast {
    vest[] = {"acp_CSAT_GHex_V_CarrierRigBW_GL_CSAT_GHex"};
    backpack[] = {"acp_CSAT_GHex_B_AssaultPackEnhanced_CSAT_GHex"};

    primaryWeapon[] = {"arifle_Katiba_GL_F"};

    class SmokeUGL : BaseUGLSmokes {};
    class UGL : BaseUGLs {};
};

// Rifleman
class O_Soldier_F : BaseClassEast {
    backpack[] = {"acp_CSAT_GHex_B_AssaultPackEnhanced_CSAT_GHex"};
};

// Rifleman-AT
class O_Soldier_LAT_F : BaseClassEast {
    backpack[] = {""};
    primaryWeapon[] = {"arifle_Katiba_C_F"};
    secondaryWeapon[] = {"launch_MRAWS_green_rail_F"};
    secondaryWeaponLoadedMagazine[] = {"MRAWS_HEAT55_F"};
};
class PistolBaseGUER : BaseCollection {
    handgun[] = {"hgun_ACPC2_F"};
    handgunLoadedMagazine[] = {"9Rnd_45ACP_Mag"};
    magazines[] = {{"9Rnd_45ACP_Mag", 1}};
};

class BaseClassGUER : BaseCollection {
    uniform[] = {"U_I_C_Soldier_Para_2_F", "U_I_C_Soldier_Para_3_F", "U_I_C_Soldier_Para_4_F", "U_I_C_Soldier_Para_1_F"};
    vest[] = {"acp_Solid_Ranger_Green_V_PlateCarrierIA1_Solid_Ranger_Green"};
    headgear[] = {"acp_Solid_Ranger_Green_H_HelmetEAST_Solid_Ranger_Green_F"};

    primaryWeapon[] = {"arifle_TRG21_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag"};
    primaryWeaponBarrel[] = {"acc_omnilight"};
    primaryWeaponOptic[] = {"optic_aco_grn"};

    magazines[] = {{"30Rnd_556x45_Stanag",6}};

    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch"};

    class Pistol : PistolBaseGUER {};
    class Smoke : BaseGrenades {};
    class ACEItems: BaseACEItems {};
};

class LeaderBaseGUER : BaseClassGUER {
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch", "ItemGPS"};
    class Smoke : BaseGrenadesLeader {};
};

// Factionleader
class I_officer_F : LeaderBaseGUER {
    uniform[] = {"U_I_C_Soldier_Camo_F"};
    vest[] = {"acp_Solid_Ranger_Green_V_PlateCarrierIA2_Solid_Ranger_Green"};
    headgear[] = {"acp_Solid_Ranger_Green_H_HelmetEAST_Cover_Solid_Ranger_Green_F"};

    binocular[] = {"Binocular"};
    items[] = {{"ACRE_BF888S",1}};
};

// Squadleader
class I_Soldier_SL_F : I_officer_F {
    uniform[] = {"U_I_C_Soldier_Para_2_F"};
    headgear[] = {"acp_Solid_Ranger_Green_H_HelmetEAST_Headset_Solid_Ranger_Green_F"};
};

// Radioman
class I_crew_F : I_officer_F {
    uniform[] = {"U_I_C_Soldier_Para_3_F"};
    vest[] = {"acp_Solid_Ranger_Green_V_PlateCarrierIA1_Solid_Ranger_Green"};
    backpack[] = {"acp_Solid_Ranger_Green_B_TacticalPack_Solid_Ranger_Green"};
    items[] = {{"ACRE_BF888S",1},{"ACRE_PRC77",1}};
};

// Automatic Rifleman
class I_Soldier_AR_F : BaseClassGUER {
    vest[] = {"acp_Solid_Ranger_Green_V_PlateCarrierIA1_Solid_Ranger_Green"};
    backpack[] = {"B_Kitbag_sgg"};
    primaryWeapon[] = {"LMG_03_F"};
    primaryWeaponLoadedMagazine[] = {"200Rnd_556x45_Box_F"};
    primaryWeaponOptic[] = {"optic_holosight_blk_f"};
    magazines[] = {{"200Rnd_556x45_Box_F",4}};
};

// Medic
class I_medic_F : BaseClassGUER {
    vest[] = {"acp_Solid_Ranger_Green_V_PlateCarrierIA1_Solid_Ranger_Green"};
    backpack[] = {"B_Kitbag_sgg"};
    class ACEItems: ACEItemsMedic {};
};

// Rifleman
class I_soldier_F : BaseClassGUER {
    vest[] = {"acp_Solid_Ranger_Green_V_PlateCarrierIA1_Solid_Ranger_Green"};
    primaryWeapon[] = {"arifle_Mk20_plain_F"};
};
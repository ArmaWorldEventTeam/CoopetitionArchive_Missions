class PistolBaseWest : BaseCollection {
    handgun[] = {"hgun_P07_F"};
    handgunLoadedMagazine[] = {"16Rnd_9x21_Mag"};
    magazines[] = {{"16Rnd_9x21_Mag", 1}};
};

class BaseClassWest : BaseCollection {
    uniform[] = {"ACM_LDF_DES_Soldier"};
    vest[] = {"ACM_V_C_MV6"};
    headgear[] = {"ACM_C_H_Helmet4"};

    primaryWeapon[] = {"arifle_MSBS65_sand_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_msbs_mag"};
    primaryWeaponBarrel[] = {"acc_pointer_ir"};
    primaryWeaponOptic[] = {"optic_holosight"};

    magazines[] = {{"30Rnd_65x39_caseless_msbs_mag",4},{"30Rnd_65x39_caseless_msbs_mag_Tracer",2}};

    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ACE_NVG_Wide"};

    class Pistol : PistolBaseWest {};
    class Smoke : BaseGrenades {};
    class ACEItems: BaseACEItems {};
};

class LeaderBaseWest : BaseClassWest {
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ACE_NVG_Wide","ItemGPS"};
    class Smoke : BaseGrenadesLeaderBlue {};
};
// Factionleader
class B_officer_F : LeaderBaseWest {
    headgear[] = {"ACM_C_H_Helmet6"};
    binocular[] = {"ACE_Vector"};
    items[] = {{"ACE_microDAGR",1},{"ACRE_PRC152",1}};
};

// Squadleader
class B_Soldier_SL_F : B_officer_F {
    headgear[] = {"ACM_C_H_Helmet7"};
};

// Radioman
class B_crew_F : B_officer_F {
    uniform[] = {"ACM_LDF_DES_Shortsleeve"};
    vest[] = {"ACM_V_C_MV1"};
    backpack[] = {"ACM_B_C_FAST"};
    items[] = {{"ACE_microDAGR",1},{"ACRE_PRC152",1},{"ACRE_PRC117F",1}};
};

// Medic
class B_medic_F : BaseClassWest {
    vest[] = {"ACM_V_C_MV1"};
    headgear[] = {"ACM_C_H_Helmet3"};
    backpack[] = {"ACM_B_C_FASTMED"};

    primaryWeaponOptic[] = {"optic_aco"};

    class ACEItems: ACEItemsMedic {};
};

// Automatic Rifleman
class B_soldier_AR_F : BaseClassWest {
    headgear[] = {"ACM_C_H_Helmet5"};
    backpack[] = {"ACM_B_C_COMPAT"};

    primaryWeapon[] = {"LMG_Mk200_black_F"};
    primaryWeaponOptic[] = {"optic_aco"};
    primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box"};
    magazines[] = {{"200Rnd_65x39_cased_Box", 4}};
};

// Grenadier
class B_Soldier_GL_F : BaseClassWest {
    uniform[] = {"ACM_LDF_DES_Shortsleeve"};
    vest[] = {"ACM_V_C_MV4"};
    headgear[] = {"ACM_C_H_Helmet2"};
    backpack[] = {"ACM_B_C_COMPAT"};

    primaryWeapon[] = {"arifle_MSBS65_GL_sand_F"};

    class SmokeUGL : BaseUGLSmokes {};
    class UGL : BaseUGLs {};
};

// Marksman
class B_soldier_M_F : BaseClassWest {
    vest[] = {"ACM_V_C_MV1"};
    headgear[] = {"ACM_C_H_Helmet3"};

    primaryWeapon[] = {"arifle_MSBS65_Mark_sand_F"};
    primaryWeaponOptic[] = {"optic_dms"};
};

// (Co-) Pilot
class B_Helipilot_F : LeaderBaseWest {
    uniform[] = {"ACM_LDF_DES_Pilot"};
    vest[] = {"ACM_V_C_MV2"};
    headgear[] = {"H_PilotHelmetHeli_B"};
    backpack[] = {"ACM_B_C_FAST"};

    primaryWeapon[] = {"SMG_03C_khaki"};
    primaryWeaponLoadedMagazine[] = {"50Rnd_570x28_SMG_03"};
    magazines[] = {{"50Rnd_570x28_SMG_03", 2}};

    items[] = {{"ACE_microDAGR",1},{"ACRE_PRC152",1},{"ACRE_PRC117F",1}};
};
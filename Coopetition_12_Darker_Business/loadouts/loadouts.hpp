class CfgCLibLoadoutsClassBase; // Import Base Class
class CfgCLibLoadouts {
    class BaseCollection : CfgCLibLoadoutsClassBase  {
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    class BaseUGLs : BaseCollection {
        magazines[] = {{"1Rnd_HE_Grenade_shell",8}};
    };

    class BaseUGLSmokes : BaseCollection {
        magazines[] = {{"1Rnd_SmokeRed_Grenade_shell",4},{"UGL_FlareWhite_F",6},{"UGL_FlareGreen_F",4},{"UGL_FlareRed_F",4}};
    };

    class BaseGrenades : BaseCollection {
        magazines[] = {{"SmokeShell",2},{"HandGrenade",2},{"ACE_Chemlight_HiWhite",4}};
    };

    class BaseGrenadesLeader : BaseCollection {
        magazines[] = {{"SmokeShell",2},{"HandGrenade",2},{"SmokeShellRed",1},{"SmokeShellGreen",1},{"ACE_Chemlight_HiWhite",4},{"ACE_HandFlare_White",2},{"ACE_HandFlare_Green",2},{"ACE_HandFlare_Red",2}};
    };

    class BaseGrenadesLeaderBlue : BaseCollection {
        magazines[] = {{"SmokeShell",4},{"HandGrenade",2},{"ACE_Chemlight_HiWhite",4},{"ACE_HandFlare_White",2},{"ACE_HandFlare_Green",2},{"ACE_HandFlare_Red",2},{"B_IR_Grenade",2}};
    };

    class BaseACEItems : BaseCollection {
        items[] = {{"ACE_tourniquet",2},{"ACE_EarPlugs",1},{"ACE_Flashlight_KSF1",1},{"ACE_MapTools",1},{"ACE_fieldDressing",5},{"ACE_quikclot",5},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_CableTie",2}};
    };

    class ACEItemsMedic : BaseCollection {
        items[] = {{"ACE_fieldDressing",20},{"ACE_elasticBandage",20},{"ACE_packingBandage",20},{"ACE_quikclot",20},{"ACE_tourniquet",8},{"ACE_salineIV_500",6},{"ACE_splint",6},{"ACE_adenosine",5},{"ACE_epinephrine",5},{"ACE_morphine",5},{"ACE_salineIV",4},{"ACE_salineIV_250",4},{"ACE_surgicalKit",1}};
    };

    #include "nato.hpp"
    #include "opfor.hpp"
    #include "guer.hpp"

    // Zeus
    class C_Soldier_VR_F : BaseCollection {
        headgear[] = {"H_Hat_Tinfoil_F"};
        binocular[] = {"Binocular"};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch", "ItemGPS"};
    };

    class C_man_pilot_F : BaseCollection {
        uniform[] = {"ACM_LDF_DES_Pilot"};
        headgear[] = {"H_HeadBandage_bloody_F","H_Booniehat_khk"};
        items[] = {{"ACE_tourniquet",1},{"ACE_EarPlugs",1},{"ACE_fieldDressing",4}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch"};
    };
};
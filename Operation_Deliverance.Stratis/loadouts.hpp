class CfgCLibLoadouts {
  class BaseCollection {
      uniform[] = {};
      vest[] = {};
      headgear[] = {};
      goggle[] = {};
      backpack[] = {};
      primaryWeapon[] = {};
      primaryWeaponOptic[] = {};
      primaryWeaponMuzzle[] = {};
      primaryWeaponBarrel[] = {};
      primaryWeaponResting[] = {};
      primaryWeaponLoadedMagazine[] = {};
      secondaryWeapon[] = {};
      secondaryWeaponOptic[] = {};
      secondaryWeaponMuzzle[] = {};
      secondaryWeaponBarrel[] = {};
      secondaryWeaponResting[] = {};
      secondaryWeaponLoadedMagazine[] = {};
      handgun[] = {};
      handgunOptic[] = {};
      handgunMuzzle[] = {};
      handgunBarrel[] = {};
      handgunResting[] = {};
      handgunLoadedMagazine[] = {};
      binocular[] = {};
      magazines[] = {};
      items[] = {};
      itemsUniform[] = {};
      itemsVest[] = {};
      itemsBackpack[] = {};
      linkedItems[] = {};
      script[] = {};
      removeAllWeapons = 0;
      removeAllItems = 0;
      removeAllAssignedItems = 0;
  };
  // AAF Basis ------------------------------------------------------------------------------------------------------
  class aaf_baseclass : BaseCollection {
      uniform[] = {"U_I_CombatUniform","U_I_CombatUniform_shortsleeve"};
      vest[] = {"V_PlateCarrierIA1_dgtl","V_PlateCarrierIA2_dgtl"};
      headgear[] = {"H_HelmetIA","H_HelmetCrew_I"};
      backpack[] = {"B_AssaultPack_dgtl","B_TacticalPack_oli"};
      handgun[] = {"hgun_ACPC2_F"};
      handgunLoadedMagazine[] = {"9Rnd_45ACP_Mag"};
      binocular[] = {"Binocular"};
      linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS","Binocular"};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",1}};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
  };

  // Zugführer
  class I_Officer_F : aaf_baseclass {
      vest[] = {"V_PlateCarrierIA2_dgtl"};
      headgear[] = {"H_MilCap_dgtl"};
      backpack[] = {"tf_anprc155"};
      handgun[] = {"hgun_Pistol_heavy_01_F"};
      handgunLoadedMagazine[] = {"11Rnd_45ACP_Mag"};
      primaryWeapon[] = {"arifle_Mk20_F"};
      primaryWeaponOptic[] = {"optic_ACO_grn"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",6},{"SmokeShellBlue",1}};
  };

  // Truppführer
  class I_Soldier_SL_F : aaf_baseclass {
      backpack[] = {"tf_anprc155"};
      handgun[] = {"hgun_Pistol_heavy_01_F"};
      handgunLoadedMagazine[] = {"11Rnd_45ACP_Mag"};
      primaryWeapon[] = {"arifle_Mk20_F"};
      primaryWeaponOptic[] = {"optic_ACO_grn"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",6},{"SmokeShellBlue",1}};
  };

  // MG Schütze
  class I_Soldier_AR_F : aaf_baseclass {
     primaryWeapon[] = {"LMG_Mk200_LP_BI_F"};
     primaryWeaponOptic[] = {"optic_Holosight"};
     primaryWeaponResting[] = {"bipod_03_F_blk"};
     primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box_Tracer"};
     magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"200Rnd_65x39_cased_Box_Tracer",3}};
  };

  //MG Assistent
  class I_Soldier_AAR_F : aaf_baseclass {
      backpack[] = {"B_TacticalPack_oli"};
      primaryWeapon[] = {"arifle_Mk20_F"};
      primaryWeaponOptic[] = {"optic_ACO_grn"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",10},{"200Rnd_65x39_cased_Box_Tracer",3}};
  };

  // Grenadier
  class I_Soldier_GL_F : aaf_baseclass {
      primaryWeapon[] = {"arifle_Mk20_GL_F"};
      primaryWeaponOptic[] = {"optic_ACO_grn"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",10},{"1Rnd_HE_Grenade_shell",10}};
  };

  // Rifleman
  class I_Soldier_F	: aaf_baseclass {
      primaryWeapon[] = {"arifle_Mk20_F"};
      primaryWeaponOptic[] = {"optic_ACO_grn"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",14}};
  };

  // UAV Operator
  class I_Soldier_UAV_F : aaf_baseclass {
      backpack[] = {"I_UAV_01_backpack_F"};
      primaryWeapon[] = {"arifle_Mk20_F"};
      primaryWeaponOptic[] = {"optic_ACO_grn"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",8}};
      linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","I_UavTerminal","Binocular"};
  };

  // Spotter Marksmen DLC
  class I_Spotter_F : aaf_baseclass {
	  uniform[] = {"U_I_FullGhillie_sard"};
      vest[] = {"V_Chestrig_oli"};
      headgear[] = {"H_Booniehat_oli"};
	  goggles[] = {"G_Bandanna_khk"};
      backpack[] = {"B_AssaultPack_dgtl"};
      primaryWeapon[] = {"srifle_DMR_03_F"};
      primaryWeaponOptic[] = {"optic_Hamr"};
      primaryWeaponResting[] = {"bipod_03_F_blk"};
      primaryWeaponMuzzle[] = {"muzzle_snds_B"};
      primaryWeaponLoadedMagazine[] = {"20Rnd_762x51_Mag"};
	  magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"20Rnd_762x51_Mag",14}};
      linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS","Laserdesignator_03"};
      items[] = {{"ACE_RangeCard",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",1}};
  };

  // Sniper Marksmen DLC
  class I_Sniper_F : aaf_baseclass {
      uniform[] = {"U_I_FullGhillie_sard"};
      vest[] = {"V_Chestrig_oli"};
      headgear[] = {"H_Booniehat_oli"};
      goggles[] = {"G_Bandanna_khk"};
      backpack[] = {"B_AssaultPack_dgtl"};
      primaryWeapon[] = {"srifle_DMR_02_F"};
      primaryWeaponOptic[] = {"optic_AMS"};
      primaryWeaponResting[] = {"bipod_03_F_blk"};
      primaryWeaponMuzzle[] = {"muzzle_snds_338_black"};
      primaryWeaponLoadedMagazine[] = {"10Rnd_338_Mag"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"10Rnd_338_Mag",12}};
      linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS","Binocular"};
  };



  // NATO Basis ------------------------------------------------------------------------------------------------------
  class nato_baseclass : BaseCollection {
      uniform[] = {"U_B_CombatUniform_mcam","U_B_CombatUniform_mcam_tshirt"};
      vest[] = {"V_PlateCarrier2_rgr","V_PlateCarrierSpec_rgr"};
      headgear[] = {"H_HelmetB_sand","H_HelmetB_black","H_HelmetB_grass"};
      backpack[] = {"B_AssaultPack_rgr"};
      handgun[] = {"hgun_P07_F"};
      handgunLoadedMagazine[] = {"16Rnd_9x21_red_Mag"};
      binocular[] = {"Binocular"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
      linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","ItemGPS","Binocular"};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",1}};

  };
  // Commander
  class B_officer_F : nato_baseclass {
      headgear[] = {"H_Beret_Colonel"};
      backpack[] = {"tf_rt1523g"};
      primaryWeapon[] = {"arifle_MX_F"};
      primaryWeaponOptic[] = {"optic_Holosight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",6},{"SmokeShellGreen",1}};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_MapTools",1}};
  };

  //SQl
  class B_Soldier_SL_F : nato_baseclass {
      backpack[] = {"tf_rt1523g"};
      primaryWeapon[] = {"arifle_MX_F"};
      primaryWeaponOptic[] = {"optic_Holosight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",6},{"SmokeShellGreen",1}};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_MapTools",1}};
  };

  // Teamleader
  class B_Soldier_TL_F : nato_baseclass {
      primaryWeapon[] = {"arifle_MX_F"};
      primaryWeaponOptic[] = {"optic_Holosight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",10},{"SmokeShellGreen",1}};
      items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_MapTools",1}};
  };

  // AR marksman dlc
  class B_soldier_AR_F : nato_baseclass {
      primaryWeapon[] = {"MMG_02_sand_F"};
      primaryWeaponOptic[] = {"optic_Holosight"};
      primaryWeaponResting[] = {"bipod_01_F_snd"};
      primaryWeaponLoadedMagazine[] = {"130Rnd_338_Mag"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"130Rnd_338_Mag",3}};
  };

  // Grenadier
  class B_Soldier_GL_F : nato_baseclass {
      primaryWeapon[] = {"arifle_MX_GL_F"};
      primaryWeaponOptic[] = {"optic_Holosight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",10}};
  };

  // Rifleman
  class B_Soldier_F	: nato_baseclass {
      primaryWeapon[] = {"arifle_MX_F"};
      primaryWeaponOptic[] = {"optic_Holosight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",14}};
  };

  // UAV Operator
  class B_soldier_UAV_F : nato_baseclass {
      backpack[] = {"B_UAV_01_backpack_F"};
      primaryWeapon[] = {"arifle_MX_F"};
      primaryWeaponOptic[] = {"optic_Holosight"};
      primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag"};
      magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",6}};
      linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","B_UavTerminal","Binocular"};
  };
};

if (isClass (configFile >> "CfgPatches" >> "zen_common")) then {
    ["AW-Support", "Teleport", {
        params ["", "_object"];
        _object execVM "functions\fnc_teleport_v2.sqf";
    }] call zen_custom_modules_fnc_register;
};
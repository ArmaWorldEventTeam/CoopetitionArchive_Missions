//teleport to group

_caller = param [0];
if (isNil "_caller") exitWith {};
_group = units group _caller;
_pretty_names = _group apply {[name _x]};
["Main Teleport", [["COMBO", "Teleport To", [_group, _pretty_names, 0]]], {
	_target = (_this select 0) select 0;
	_caller = (_this select 1) select 0;
	if ( vehicle _target == _target)then {
		//novehicle
		_caller setVehiclePosition [getPos _target, [], 0, "NONE"];
	}
	else{
		//vehicle
		_caller moveInAny vehicle _target;
	};
},{},[_caller]] call ZEN_dialog_fnc_create;

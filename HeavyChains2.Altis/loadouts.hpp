class CfgCLibLoadouts {
    class BaseCollection {
        uniform[] = {};
        vest[] = {};
        headgear[] = {};
        goggle[] = {};
        backpack[] = {};
        primaryWeapon[] = {};
        primaryWeaponOptic[] = {};
        primaryWeaponMuzzle[] = {};
        primaryWeaponBarrel[] = {};
        primaryWeaponResting[] = {};
        primaryWeaponLoadedMagazine[] = {};
        secondaryWeapon[] = {};
        secondaryWeaponOptic[] = {};
        secondaryWeaponMuzzle[] = {};
        secondaryWeaponBarrel[] = {};
        secondaryWeaponResting[] = {};
        secondaryWeaponLoadedMagazine[] = {};
        handgun[] = {};
        handgunOptic[] = {};
        handgunMuzzle[] = {};
        handgunBarrel[] = {};
        handgunResting[] = {};
        handgunLoadedMagazine[] = {};
        binocular[] = {};
        magazines[] = {};
        items[] = {};
        itemsUniform[] = {};
        itemsVest[] = {};
        itemsBackpack[] = {};
        linkedItems[] = {};
        script[] = {};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };
	
    //AAF Grundausrüstung
    class aaf_baseclass : BaseCollection {
        uniform[] = {"U_I_CombatUniform","U_I_CombatUniform_shortsleeve"};
        vest[] = {"V_PlateCarrierIA1_dgtl"};
        headgear[] = {"H_HelmetIA"};
        backpack[] = {"B_AssaultPack_dgtl"};
		primaryWeapon[] = {"arifle_Mk20C_F"};
        primaryWeaponOptic[] = {"optic_ACO","optic_holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_red"};
        handgun[] = {"hgun_ACPC2_F"};
        handgunLoadedMagazine[] = {"9Rnd_45ACP_Mag"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC343",1}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS"};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    //AAF Officer
    class I_officer_F : aaf_baseclass {
		primaryWeapon[] = {"SMG_01_F"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_45ACP_Mag_SMG_01"};
        headgear[] = {"H_Beret_blk"};
        binocular[] = {"Binocular"};
        backpack[] = {"B_Kitbag_rgr"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_45ACP_Mag_SMG_01",10}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC117F",1},{"H_Cap_blk_Raven"},{"ACRE_PRC343",1},{"H_HelmetCrew_I",1}};
    };

    //AAF Commander
    class I_Soldier_TL_F : aaf_baseclass {
		primaryWeapon[] = {"SMG_01_F"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_45ACP_Mag_SMG_01"};
		headgear[] = {"H_HelmetCrew_I"};
        backpack[] = {"B_Kitbag_rgr"};
        binocular[] = {"Binocular"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"SmokeShellRed",1},{"SmokeShellYellow",1},{"9Rnd_45ACP_Mag",2},{"30Rnd_45ACP_Mag_SMG_01",10},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeYellow_Grenade_shell",2}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC148",1},{"ACRE_PRC117F",1},{"ACRE_PRC343",1}};
	};

    //AAF Crewman
    class I_crew_F : aaf_baseclass {
		primaryWeapon[] = {"SMG_01_F"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_45ACP_Mag_SMG_01"};
        headgear[] = {"H_HelmetCrew_I"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_45ACP_Mag_SMG_01",10}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC148",1},{"ACRE_PRC343",1}};
    };

    //AAF FTL
    class I_Soldier_SL_F : aaf_baseclass {
		vest[] = {"V_PlateCarrierIA2_dgtl"};
        primaryWeapon[] = {"arifle_Mk20_GL_F"};
		backpack[] = {"B_Kitbag_rgr"};
        binocular[] = {"Binocular"};
		secondaryWeapon[] = {"launch_MRAWS_green_F"};
        secondaryWeaponLoadedMagazine[] = {"MRAWS_HEAT_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_red",10},{"1Rnd_HE_Grenade_shell",6},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeYellow_Grenade_shell",2},{"MRAWS_HEAT_F",2}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC148",1},{"ACRE_PRC343",1}};
		};

    //AAF LMG
    class I_Soldier_AR_F : aaf_baseclass {
        primaryWeapon[] = {"LMG_Mk200_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
		primaryWeaponResting[] = {"bipod_03_F_blk"};
        primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box_Red"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"200Rnd_65x39_cased_Box_Red",3}};
    };

    //AAF Schütze AT
    class I_Soldier_AT_F : aaf_baseclass {
        secondaryWeapon[] = {"launch_O_Vorona_green_F"};
        secondaryWeaponLoadedMagazine[] = {"Vorona_HEAT"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_red",10},{"Vorona_HEAT",2},{"Vorona_HE",1}};
    };

    // AAF Hilfsschütze AT
    class I_Soldier_AAT_F : aaf_baseclass {
		backpack[] = {"B_Carryall_oli"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_red",10},{"Vorona_HEAT",2},{"Vorona_HE",1}};
    };

    //AAF Grenadier
    class I_Soldier_GL_F : aaf_baseclass {
		vest[] = {"V_PlateCarrierIA2_dgtl"};
        primaryWeapon[] = {"arifle_Mk20_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_red"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_red",10},{"1Rnd_HE_Grenade_shell",6},{"1Rnd_Smoke_Grenade_shell",5}};
    };
	
	//AAF Rifleman
    class I_soldier_F : aaf_baseclass {
		backpack[] = {"B_Carryall_oli"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",4},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_red",14},{"200Rnd_65x39_cased_Box_Red",2}};
		items [] = {{"ACE_EntrenchingTool",1},{"ACE_EarPlugs",1},{"ACE_morphine",6},{"ACE_fieldDressing",20},{"ACE_CableTie",6},{"ACRE_PRC343",1}};
    };

	//AAF Scout TL
    class I_diver_TL_F : aaf_baseclass {
        goggle[] = {"G_Bandanna_beast"};
        primaryWeapon[] = {"arifle_Mk20_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_red"};
		primaryWeaponMuzzle[] = {"muzzle_snds_m"};
		binocular[] = {"Rangefinder"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_red",10}};
		items [] = {{"ACE_EntrenchingTool",1},{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC343",1},{"ACRE_PRC117F",1}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","Laserdesignator"};
    };

	//AAF Scout
    class I_diver_F : aaf_baseclass {
        primaryWeapon[] = {"arifle_Mk20_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_red"};
		primaryWeaponMuzzle[] = {"muzzle_snds_m"};
        secondaryWeapon[] = {"launch_O_Vorona_green_F"};
        secondaryWeaponLoadedMagazine[] = {"Vorona_HEAT"};
		binocular[] = {"Rangefinder"};
        goggle[] = {"G_Bandanna_beast"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_red",10},{"Vorona_HEAT",1}};
        items[] = {{"ACE_EntrenchingTool",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC343",1}};
    };

	//AAF Logistiker TL
    class I_Soldier_A_F : aaf_baseclass {
        backpack[] = {"B_Kitbag_rgr"};
        primaryWeapon[] = {"arifle_Mk20C_F"};
        primaryWeaponOptic[] = {""};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_red"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_red",10}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ToolKit",1},{"ACRE_PRC117F",1},{"ACRE_PRC148",1}};
    };

	//AAF Logistiker
    class I_engineer_F : aaf_baseclass {
		backpack[] = {"B_Carryall_oli"};
        primaryWeapon[] = {"arifle_Mk20C_F"};
        primaryWeaponOptic[] = {""};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_red"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",6},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_red",14}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ToolKit",1},{"ACRE_PRC148",1},{"ACRE_PRC343",1}};
    };



	//------------------	NATO	------------------

    //NATO Grundausrüstung
    class nato_baseclass : BaseCollection {
        uniform[] = {"U_B_CombatUniform_mcam","U_B_CombatUniform_mcam_tshirt"};
        vest[] = {"V_PlateCarrier1_rgr"};
        goggle[] = {"G_Aviator","",""};
        headgear[] = {"H_HelmetB","H_HelmetB_black","H_HelmetB_desert"};
        backpack[] = {"B_AssaultPack_khk","B_AssaultPack_rgr"};
        primaryWeapon[] = {"arifle_TRG21_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_green"};
        handgun[] = {"hgun_Pistol_heavy_01_F"};
        handgunLoadedMagazine[] = {"11Rnd_45ACP_Mag"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_green",5}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC343",1}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS"};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    //NATO Officer
    class O_officer_F : nato_baseclass {
        headgear[] = {"H_HelmetCrew_B"};
        primaryWeapon[] = {"SMG_03C_black"};
        primaryWeaponLoadedMagazine[] = {"50Rnd_570x28_SMG_03"};
        binocular[] = {"Binocular"};
        backpack[] = {"B_Kitbag_mcamo"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"50Rnd_570x28_SMG_03",5}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"H_Beret_02"},{"ACRE_PRC117F",1},{"ACRE_PRC343",1}};
		};

    //NATO Commander
    class O_Soldier_TL_F : nato_baseclass {
        headgear[] = {"H_HelmetCrew_B"};
		backpack[] = {"B_Kitbag_mcamo"};
        primaryWeapon[] = {"arifle_TRG21_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn","optic_holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_green"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"SmokeShellRed",2},{"SmokeShellYellow",1},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_green",4},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",3},{"1Rnd_SmokeYellow_Grenade_shell",3}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC117F",1},{"ACRE_PRC148",1},{"ACRE_PRC343",1}};
		};

    //NATO Crewman
    class O_crew_F : nato_baseclass {
        headgear[] = {"H_HelmetCrew_B"};
		backpack[] = {"B_Kitbag_mcamo"};
        primaryWeapon[] = {"SMG_03C_black"};
        primaryWeaponLoadedMagazine[] = {"50Rnd_570x28_SMG_03"};
		magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"50Rnd_570x28_SMG_03",5}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC148",1},{"ACRE_PRC343",1}};
    };

    //NATO FTL
    class O_Soldier_SL_F : nato_baseclass {
        primaryWeapon[] = {"arifle_TRG21_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn","optic_holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_green"};
		backpack[] = {"B_Kitbag_mcamo"};
		secondaryWeapon[] = {"launch_MRAWS_sand_F"};
        secondaryWeaponLoadedMagazine[] = {"MRAWS_HEAT_F"};		
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_green",6},{"1Rnd_Smoke_Grenade_shell",4},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeYellow_Grenade_shell",2},{"MRAWS_HEAT_F",2}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC148",1},{"ACRE_PRC343",1}};
		};

    //NATO LMG
    class O_Soldier_AR_F : nato_baseclass {
        primaryWeapon[] = {"LMG_Zafir_F"};
        primaryWeaponOptic[] = {""};
        primaryWeaponLoadedMagazine[] = {"150Rnd_762x54_Box"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"150Rnd_762x54_Box",4}};
    };

    //NATO Schütze AT
    class O_Soldier_LAT_F : nato_baseclass {
        secondaryWeapon[] = {"launch_O_Vorona_green_F"};
        secondaryWeaponLoadedMagazine[] = {"Vorona_HEAT"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_green",6},{"Vorona_HEAT",2},{"Vorona_HE",1}};
    };

    //NATO Hilfsschütze AT
    class O_Soldier_AAT_F : nato_baseclass {
		backpack[] = {"B_Carryall_mcamo"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_green",6},{"Vorona_HEAT",2},{"Vorona_HE",1}};
    };

    //NATO Grenadier
    class O_Soldier_GL_F : nato_baseclass {
		vest[] = {"V_PlateCarrierGL_mtp"};
        primaryWeapon[] = {"arifle_TRG21_GL_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_green",6},{"1Rnd_HE_Grenade_shell",6},{"1Rnd_Smoke_Grenade_shell",3}};
    };

    //NATO Rifleman
    class O_soldier_F : nato_baseclass {
		backpack[] = {"B_Carryall_mcamo"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",6},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_green",12},{"150Rnd_762x54_Box",2},{"1Rnd_HE_Grenade_shell",4},{"1Rnd_Smoke_Grenade_shell",4}};
		items [] = {{"ACE_EntrenchingTool",1},{"ACE_EarPlugs",1},{"ACE_morphine",6},{"ACE_fieldDressing",20},{"ACE_CableTie",6},{"ACRE_PRC343",1}};
    };

    //NATO Scout TL
    class O_diver_TL_F : nato_baseclass {
        goggle[] = {"G_Bandanna_beast"};
		binocular[] = {"Rangefinder"};
		backpack[] = {"B_Kitbag_mcamo"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",6},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_green",6}};
        items [] = {{"ACE_EntrenchingTool",1},{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC117F",1},{"ACRE_PRC343",1}};
    };

    //NATO Scout
    class O_diver_F : nato_baseclass {
        goggle[] = {"G_Bandanna_beast"};
		backpack[] = {"O_Static_Designator_01_weapon_F"};
		binocular[] = {"Rangefinder"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",6},{"11Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_green",6}};
		items [] = {{"ACE_EntrenchingTool",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACRE_PRC343",1}};
		linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","O_UavTerminal"};
    };

    //NATO Logistiker TL
    class O_Soldier_A_F : nato_baseclass {
		backpack[] = {"B_Kitbag_mcamo"};
        primaryWeapon[] = {"SMG_03C_black"};
        primaryWeaponLoadedMagazine[] = {"50Rnd_570x28_SMG_03"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",6},{"11Rnd_45ACP_Mag",2},{"50Rnd_570x28_SMG_03",6},{"1Rnd_Smoke_Grenade_shell",6}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ToolKit",1},{"ACRE_PRC117F",1},{"ACRE_PRC148",1}};
    };

    //NATO Logistiker
    class O_engineer_F : nato_baseclass {
		backpack[] = {"B_Carryall_mcamo"};
        primaryWeapon[] = {"SMG_03C_black"};
        primaryWeaponLoadedMagazine[] = {"50Rnd_570x28_SMG_03"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"50Rnd_570x28_SMG_03",3}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ToolKit",1},{"ACRE_PRC148",1},{"ACRE_PRC343",1}};
    };
};
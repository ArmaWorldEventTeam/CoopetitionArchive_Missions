class CfgCLibLoadouts {

    class BaseCollection {
        uniform[] = {};
        vest[] = {};
        headgear[] = {};
        goggle[] = {};
        backpack[] = {};
        primaryWeapon[] = {};
        primaryWeaponOptic[] = {};
        primaryWeaponMuzzle[] = {};
        primaryWeaponBarrel[] = {};
        primaryWeaponResting[] = {};
        primaryWeaponLoadedMagazine[] = {};
        secondaryWeapon[] = {};
        secondaryWeaponOptic[] = {};
        secondaryWeaponMuzzle[] = {};
        secondaryWeaponBarrel[] = {};
        secondaryWeaponResting[] = {};
        secondaryWeaponLoadedMagazine[] = {};
        handgun[] = {};
        handgunOptic[] = {};
        handgunMuzzle[] = {};
        handgunBarrel[] = {};
        handgunResting[] = {};
        handgunLoadedMagazine[] = {};
        binocular[] = {};
        magazines[] = {};
        items[] = {};
        itemsUniform[] = {};
        itemsVest[] = {};
        itemsBackpack[] = {};
        linkedItems[] = {};
        script[] = {};
        removeAllWeapons = 0;
        removeAllItems = 0;
        removeAllAssignedItems = 0;
    };

    //MIM Grundausrüstung
    class mim_baseclass : BaseCollection {
        uniform[] = {"U_I_CombatUniform","U_I_CombatUniform_shortsleeve"};
        vest[] = {"V_PlateCarrierIA1_dgtl"};
        headgear[] = {"H_HelmetIA"};
        backpack[] = {"B_AssaultPack_dgtl"};
        handgun[] = {"hgun_ACPC2_F"};
        handgunLoadedMagazine[] = {"9Rnd_45ACP_Mag"};
        binocular[] = {"Binocular"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_fadak","ItemGPS"};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    //MIM Rifleman
    class O_Soldier_F : mim_baseclass {
        primaryWeapon[] = {"arifle_Katiba_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10}};
    };

    //MIM Grenadier
    class O_Soldier_GL_F : mim_baseclass {
        primaryWeapon[] = {"arifle_Katiba_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_DefusalKit",1}};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",10},{"1Rnd_Smoke_Grenade_shell",5}};
    };

    //MIM Schütze AT
    class O_Soldier_LAT_F : mim_baseclass {
        primaryWeapon[] = {"arifle_Katiba_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        secondaryWeapon[] = {"launch_MRAWS_green_F"};
        secondaryWeaponLoadedMagazine[] = {"MRAWS_HEAT_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"MRAWS_HEAT_F",1}};
    };

    //MIM MG Schütze
    class O_Soldier_AR_F : mim_baseclass {
        primaryWeapon[] = {"LMG_Mk200_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"200Rnd_65x39_cased_Box_Tracer",3}};
    };

    //MIM Medic
    class O_medic_F : mim_baseclass {
        primaryWeapon[] = {"arifle_Katiba_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",10},{"ACE_fieldDressing",30},{"ACE_bloodIV",1},{"ACE_bloodIV_250",5},{"ACE_bloodIV_500",2},{"ACE_epinephrine",8},{"ACE_CableTie",2}};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10}};
    };

    //MIM Crewman
    class O_crew_F : mim_baseclass {
        headgear[] = {"H_HelmetCrew_I"};
        primaryWeapon[] = {"arifle_Katiba_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        itemsBackpack[] = {"ToolKit"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10}};
        unitTrait[] = {{"engineer", "true"}};
    };

    //MIM FTL
    class O_Soldier_TL_F : mim_baseclass {
        primaryWeapon[] = {"arifle_Katiba_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",8},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeYellow_Grenade_shell",2}};
    };

    //MIM SQL
    class O_Soldier_SL_F : mim_baseclass {
        backpack[] = {"tf_mr3000_multicam"};
        primaryWeapon[] = {"arifle_Katiba_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",8},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeYellow_Grenade_shell",2}};
    };

    //MIM Off
    class O_officer_F : mim_baseclass {
        uniform[] = {"U_I_OfficerUniform"};
        vest[] = {"V_BandollierB_oli"};
        headgear[] = {"H_MilCap_dgtl"};
        backpack[] = {"tf_mr3000_multicam"};
        primaryWeapon[] = {"arifle_Katiba_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10}};
    };

    //MIM Scout FTL
    class O_recon_TL_F : mim_baseclass {
        goggle[] = {"G_Balaclava_oli"};
        backpack[] = {"tf_mr3000_multicam"};
        primaryWeapon[] = {"arifle_Katiba_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponMuzzle[] = {muzzle_snds_B};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        handgunMuzzle[] = {"muzzle_snds_acp"};
        binocular[] = {"Laserdesignator"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10}};

    };

    //MIM Scout Rifelman
    class O_recon_F : mim_baseclass {
        goggle[] = {"G_Balaclava_oli"};
        primaryWeapon[] = {"arifle_Katiba_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponMuzzle[] = {"muzzle_snds_B"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        handgunMuzzle[] = {"muzzle_snds_acp"};
        binocular[] = {"Laserdesignator"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10}};
    };

    //Green Grundausrüstung ----------------------------------------------------------------------------------------------------------------------------------
    class gre_baseclass : BaseCollection {
        uniform[] = {"U_BG_Guerilla2_1","U_BG_Guerilla2_3","U_BG_Guerilla2_2","U_BG_Guerilla1_1","U_BG_Guerilla3_1","U_BG_Guerrilla_6_1","U_C_HunterBody_grn","U_I_G_resistanceLeader_F"};
        vest[] = {"V_BandollierB_rgr","V_BandollierB_khk","V_TacVest_brn","V_I_G_resistanceLeader_F","V_TacVest_camo","V_TacVestIR_blk"};
        headgear[] = {"H_Bandanna_surfer","H_Bandanna_khk_hs","H_StrawHat","H_Booniehat_oli","H_Beret_blk","H_Bandanna_camo","H_Bandanna_surfer_blk","H_Bandanna_surfer_grn","H_Bandanna_sand","H_Hat_grey","H_Cap_oli"};
        backpack[] = {"B_TacticalPack_blk","B_FieldPack_oli","B_AssaultPack_rgr","B_Kitbag_sgg"};
        binocular[] = {"Binocular"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",2},{"ACE_CableTie",2}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem"};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    // Green Platoon Leader
     class I_officer_F : gre_baseclass {
        uniform[] = {"U_NikosAgedBody"};
        vest[] = {""};
        backpack[] = {"tf_anprc155_coyote"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS"};
    };

    // Green Squadleader
     class I_Soldier_SL_F : gre_baseclass {
        backpack[] = {"tf_anprc155_coyote"};
        primaryWeapon[] = {"arifle_AKM_F"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_762x39_Mag_Tracer_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_762x39_Mag_Tracer_F",5}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS"};
    };

    // Green Fireteamleader
     class I_Soldier_TL_F : gre_baseclass {
        handgun[] = {"hgun_Pistol_heavy_02_F"};
        handgunLoadedMagazine[] = {"6Rnd_45ACP_Cylinder"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"6Rnd_45ACP_Cylinder",5}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS"};
    };

    // Green rifleman 1
     class I_soldier_F : gre_baseclass {
        handgun[] = {"hgun_Pistol_heavy_02_F"};
        handgunLoadedMagazine[] = {"6Rnd_45ACP_Cylinder"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"6Rnd_45ACP_Cylinder",5}};
    };

    // Green rifleman 2
     class I_Soldier_lite_F : gre_baseclass {
        primaryWeapon[] = {"srifle_DMR_06_olive_F"};
        primaryWeaponLoadedMagazine[] = {"20Rnd_762x51_Mag"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"20Rnd_762x51_Mag",5}};
    };

    // Green rifleman 3
     class I_Soldier_unarmed_F : gre_baseclass {
        handgun[] = {"hgun_Pistol_heavy_02_F"};
        handgunLoadedMagazine[] = {"6Rnd_45ACP_Cylinder"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"6Rnd_45ACP_Cylinder",5}};
    };

    // Green rifleman 4
     class I_Soldier_M_F : gre_baseclass {
        primaryWeapon[] = {"SMG_05_F"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_9x21_Mag_SMG_02"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"30Rnd_9x21_Mag_SMG_02",5}};
    };


    // Green rifleman AT
     class I_Soldier_LAT_F : gre_baseclass {
        handgun[] = {"hgun_Pistol_heavy_02_F"};
        handgunLoadedMagazine[] = {"6Rnd_45ACP_Cylinder"};
        secondaryWeapon[] = {"launch_RPG7_F"};
        secondaryWeaponLoadedMagazine[] = {"RPG7_F"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_762x39_Mag_Tracer_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"6Rnd_45ACP_Cylinder",5},{"RPG7_F",3}};

    };

    // Green grenadier
     class I_Soldier_GL_F : gre_baseclass {
        primaryWeapon[] = {"arifle_AK12_GL_F"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_762x39_Mag_Tracer_F"};
        magazines[] = {{"SmokeShell",2},{"30Rnd_762x39_Mag_Tracer_F",5},{"1Rnd_HE_Grenade_shell",5}};
    };

    // Green medic
     class I_medic_F : gre_baseclass {
        uniform[] = {"U_Marshal"};
        vest[] = {""};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",10},{"ACE_fieldDressing",25},{"ACE_bloodIV_250",5},{"ACE_epinephrine",10}};
        magazines[] = {{"SmokeShell",4}};
    };

    //CTRG Grundgerüst -------------------------------------------------------------------------------------------------------------
    class ctrg_baseclass : BaseCollection {
        uniform[] = {"U_B_CTRG_1","U_B_CTRG_3"};
        vest[] = {"V_PlateCarrierL_CTRG"};
        headgear[] = {"H_HelmetB_sand","H_HelmetB_black","H_HelmetB_grass"};
        backpack[] = {"B_AssaultPack_rgr"};
        handgun[] = {"hgun_P07_F"};
        handgunLoadedMagazine[] = {"16Rnd_9x21_red_Mag"};
        binocular[] = {"Binocular"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","ItemGPS"};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",1}};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    // CTRG CO_XO
    class B_officer_F : ctrg_baseclass {
        uniform[] = {"U_B_CTRG_2"};
        vest[] = {"V_Chestrig_rgr"};
        headgear[] = {"H_MilCap_gry"};
        backpack[] = {"tf_rt1523g"};
        primaryWeapon[] = {"arifle_MXC_F"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",2},{"SmokeShellGreen",1}};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_MapTools",1}};
    };

    // CTRG SQL
    class B_Soldier_SL_F : ctrg_baseclass {
        uniform[] = {"U_B_CTRG_2"};
        vest[] = {"V_PlateCarrierL_CTRG"};
        headgear[] = {"H_HelmetB_black"};
        backpack[] = {"tf_rt1523g"};
        primaryWeapon[] = {"arifle_MX_GL_F"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
        magazines[] = {{"30Rnd_65x39_caseless_mag_Tracer",10},{"16Rnd_9x21_red_Mag",2},{"MiniGrenade",2},{"SmokeShell",2},{"SmokeShellRed",1},{"SmokeShellGreen",1},{"1Rnd_SmokeGreen_Grenade_shell",2},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_Smoke_Grenade_shell",1}};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_MapTools",1}};
    };

    // CTRG AR
    class B_soldier_AR_F : ctrg_baseclass {
        primaryWeapon[] = {"arifle_MX_SW_F"};
        primaryWeaponResting[] = {"bipod_01_F_snd"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        primaryWeaponLoadedMagazine[] = {"100Rnd_65x39_caseless_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"100Rnd_65x39_caseless_mag_Tracer",6}};
    };

    //CTRG  CM
    class B_medic_F : ctrg_baseclass {
        primaryWeapon[] = {"arifle_MX_F"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
        items[] = {{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",10},{"ACE_fieldDressing",30},{"ACE_bloodIV",1},{"ACE_bloodIV_250",5},{"ACE_bloodIV_500",2},{"ACE_epinephrine",8}};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",10}};
    };

    //CTRG  Crewman
    class B_crew_F : ctrg_baseclass {
        vest[] = {"V_Chestrig_rgr"};
        headgear[] = {"H_HelmetCrew_B"};
        primaryWeapon[] = {"SMG_01_F"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_45ACP_Mag_SMG_01_Tracer_Red"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        itemsBackpack[] = {"ToolKit"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_45ACP_Mag_SMG_01_Tracer_Red",3}};
        unitTrait[] = {{"engineer", "true"}};
    };

    //CTRG  FTL
    class B_Soldier_TL_F : ctrg_baseclass {
        primaryWeapon[] = {"arifle_MX_F"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",10},{"SmokeShellRed",1},{"SmokeShellGreen",1}};
        items[] = {{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_microDAGR"}};
    };

    //CTRG  Gren
    class B_Soldier_GL_F : ctrg_baseclass {
        primaryWeapon[] = {"arifle_MX_GL_F"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
        items[] = {{"ACE_fieldDressing",4},{"ACE_CableTie",2},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_MapTools",1},{"ACE_DefusalKit",1}};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",10}};
    };

    //CTRG  LAT
    class B_soldier_LAT_F : ctrg_baseclass {
        primaryWeapon[] = {"arifle_MX_F"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
        secondaryWeapon[] = {"launch_MRAWS_sand_rail_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",7},{"MRAWS_HEAT_F",2},{"MRAWS_HE_F",1}};
    };

    //CTRG  Rfl
    class B_Soldier_F : ctrg_baseclass {
        primaryWeapon[] = {"arifle_MX_F"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",14},{"100Rnd_65x39_caseless_mag_Tracer",2}};
    };

    //CTRG  TL_Scout
    class B_recon_TL_F : ctrg_baseclass {
        uniform[] = {"U_B_CTRG_1"};
        headgear[] = {"H_HelmetB_black"};
        backpack[] = {"tf_rt1523g"};
        primaryWeapon[] = {"arifle_MX_Black_F"};
        primaryWeaponMuzzle[] = {"muzzle_snds_H"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
        handgunMuzzle[] = {"muzzle_snds_L"};
        binocular[] = {"Laserdesignator"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",8}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","ItemGPS","Laserdesignator"};
    };

    //CTRG  Scout
    class B_recon_F : ctrg_baseclass {
        uniform[] = {"U_B_CTRG_1"};
        headgear[] = {"H_HelmetB_black"};
        primaryWeapon[] = {"arifle_MX_Black_F"};
        primaryWeaponOptic[] = {"optic_Holosight"};
        primaryWeaponMuzzle[] = {"muzzle_snds_H"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_mag_Tracer"};
        handgunMuzzle[] = {"muzzle_snds_L"};
        binocular[] = {"Laserdesignator"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_red_Mag",2},{"30Rnd_65x39_caseless_mag_Tracer",8}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc152","ItemGPS"};
    };


    // IDAP Leader --------------------------------------------------------------------------------------------------
    class C_IDAP_Man_AidWorker_01_F : BaseCollection {
        uniform[] = {"U_C_IDAP_Man_cargo_F"};
        vest[] = {"V_Plain_medical_F"};
        headgear[] = {"H_PASGT_basic_white_F"};
        goggle[] = {"G_WirelessEarpiece_F"};
        backpack[] = {"B_LegStrapBag_coyote_F"};
        binocular[] = {"Binocular"};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",2},{"ACE_fieldDressing",5}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS", "tf_anprc148jem"};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    class C_IDAP_Man_AidWorker_02_F : BaseCollection {
        uniform[] = {"U_C_IDAP_Man_Jeans_F","U_C_IDAP_Man_TeeShorts_F","U_C_IDAP_Man_casual_F"};
        vest[] = {"V_Safety_yellow_F","V_Safety_blue_F","V_Safety_orange_F"};
        headgear[] = {"H_PASGT_basic_white_F"};
        goggle[] = {"G_WirelessEarpiece_F"};
        backpack[] = {"B_LegStrapBag_black_F"};
        binocular[] = {"Binocular"};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",2},{"ACE_fieldDressing",5}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS", "tf_anprc148jem"};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    class C_IDAP_Man_Paramedic_01_F : BaseCollection {
        uniform[] = {"U_C_Paramedic_01_F"};
        vest[] = {"V_Plain_crystal_F"};
        headgear[] = {"H_PASGT_basic_white_F"};
        goggle[] = {"G_WirelessEarpiece_F"};
        backpack[] = {"B_Messenger_IDAP_F"};
        binocular[] = {"Binocular"};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",10},{"ACE_fieldDressing",30},{"ACE_bloodIV",1},{"ACE_bloodIV_250",5},{"ACE_bloodIV_500",2},{"ACE_epinephrine",8},{"ACE_CableTie",2}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS", "tf_anprc148jem"};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };
};

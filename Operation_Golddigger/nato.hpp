
// GB Basis ------------------------------------------------------------------------------------------------------
class nato_baseclass : BaseCollection {
    uniform[] = {"U_B_UBACS_wdl_f", "U_B_UBACS_vest_wdl_f"};
    vest[] = {"V_PlateCarrier1_oli"};
    headgear[] = {"H_HelmetB_camo_wdl"};
    backpack[] = {"B_AssaultPack_wdl_F"};

    primaryWeapon[] = {"arifle_SA80_blk_F"};
    primaryWeaponOptic[] = {"optic_ACO_grn_AK_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_khaki_mag_Tracer"};

    handgun[] = {"hgun_Pistol_heavy_01_green_F"};
    handgunLoadedMagazine[] = {"11Rnd_45ACP_Mag"};

    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS"};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",2}};
};

// Company Leader/assi
class B_officer_F : nato_baseclass {
    headgear[] = {"H_Beret_brn_SF"};
    binocular[] = {"Binocular"};

    itemsBackpack[] = {{"ACRE_PRC117F",1}};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",6},{"SmokeShellGreen",1}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACRE_PRC152",1},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",2},{"ACE_MapTools",1}};
};

//platton Leader/assi/Sql
class B_Soldier_SL_F : nato_baseclass {
    itemsBackpack[] = {{"ACRE_PRC117F",1}};
    binocular[] = {"Binocular"};

    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",10},{"SmokeShellGreen",1}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACRE_PRC152",1},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",2},{"ACE_MapTools",1}};
};

// ftl
class B_Soldier_TL_F : nato_baseclass {
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",10},{"SmokeShellGreen",1}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACRE_PRC152",1},{"ACE_morphine",2},{"ACE_MapTools",1}};
};

// Grenadier
class B_Soldier_GL_F : nato_baseclass {
    primaryWeapon[] = {"arifle_SA80_GL_blk_F"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",5}};
};

// Rifleman
class B_Soldier_F : nato_baseclass {
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",14}};
};

// Autorifleman
class B_soldier_AR_F : nato_baseclass {
    primaryWeapon[] = {"LMG_03_F"};
    primaryWeaponOptic[] = {};
    primaryWeaponLoadedMagazine[] = {"200Rnd_556x45_Box_Red_F"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"200Rnd_556x45_Box_Red_F",4}};
};

// marksman
class B_soldier_M_F : nato_baseclass {
    primaryWeapon[] = {"arifle_SPAR_03_blk_F"};
    primaryWeaponLoadedMagazine[] = {"ACE_20Rnd_762x51_Mag_Tracer"};
    primaryWeaponOptic[] = {"optic_MRCO"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"ACE_20Rnd_762x51_Mag_Tracer",10}};
};

// mmg schütze
class B_HeavyGunner_F : nato_baseclass {
    backpack[] = {"B_Kitbag_wdl_F"};
    primaryWeapon[] = {"MMG_02_black_F"};
    primaryWeaponLoadedMagazine[] = {"130Rnd_338_Mag"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"130Rnd_338_Mag",5}};
};

// mmg assi
class B_soldier_AAR_F : nato_baseclass {
    backpack[] = {"B_Kitbag_wdl_F"};
    magazines[] = {{"SmokeShell",2},{"11Rnd_45ACP_Mag",2},{"30Rnd_65x39_caseless_khaki_mag_Tracer",7},{"130Rnd_338_Mag",6}};
    binocular[] = {"Binocular"};
};


// russ Basis ------------------------------------------------------------------------------------------------------
class csat_baseclass : BaseCollection {
    uniform[] = {"U_O_R_Gorka_01_camo_F"};
    vest[] = {"V_CarrierRigKBT_01_light_Olive_F"};
    headgear[] = {"H_HelmetAggressor_cover_F"};
    backpack[] = {"B_AssaultPack_taiga_F"};

    primaryWeapon[] = {"arifle_AK12_F"};
    primaryWeaponOptic[] = {"optic_ACO_grn_AK_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_762x39_Mag_Tracer_Green_F"};

    handgun[] = {"hgun_Rook40_F"};
    handgunLoadedMagazine[] = {"17Rnd_9x21_Mag"};

    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemGPS"};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",2}};
};

// Company Leader/assi
class O_officer_F : csat_baseclass {
    headgear[] = {"H_Beret_grn"};
    itemsBackpack[] = {{"ACRE_PRC117F",1}};
    binocular[] = {"Binocular"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_762x39_Mag_Tracer_Green_F",6},{"SmokeShellGreen",1}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACRE_PRC152",1},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",2},{"ACE_MapTools",1}};
};

//platton Leader/assi/Sql
class O_Soldier_SL_F : csat_baseclass {
    itemsBackpack[] = {{"ACRE_PRC117F",1}};
    binocular[] = {"Binocular"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_762x39_Mag_Tracer_Green_F",10},{"SmokeShellGreen",1}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACRE_PRC152",1},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACE_morphine",2},{"ACE_MapTools",1}};
};

// ftl
class O_Soldier_TL_F : csat_baseclass {
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_762x39_Mag_Tracer_Green_F",10},{"SmokeShellGreen",1}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_EarPlugs",1},{"ACRE_PRC343",1},{"ACRE_PRC152",1},{"ACE_morphine",1},{"ACE_MapTools",1}};
};

// Grenadier
class O_Soldier_GL_F : csat_baseclass {
    primaryWeapon[] = {"arifle_AK12_GL_F"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_762x39_Mag_Tracer_Green_F",10},{"1Rnd_HE_Grenade_shell",5}};
};

// Rifleman
class O_Soldier_F : csat_baseclass {
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_762x39_Mag_Tracer_Green_F",14}};
};

// Autorifleman
class O_Soldier_AR_F : csat_baseclass {
    primaryWeapon[] = {"arifle_RPK12_F"};
    primaryWeaponLoadedMagazine[] = {"75rnd_762x39_AK12_Mag_Tracer_F"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"75rnd_762x39_AK12_Mag_Tracer_F",10}};
};

// marksman
class O_soldier_M_F : csat_baseclass {
    primaryWeapon[] = {"srifle_DMR_01_black_F"};
    primaryWeaponOptic[] = {"optic_MRCO"};
    primaryWeaponLoadedMagazine[] = {"ACE_10Rnd_762x54_Tracer_mag"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"ACE_10Rnd_762x54_Tracer_mag",21}};
};

// mmg schütze
class O_HeavyGunner_F : csat_baseclass {
    backpack[] = {"B_Kitbag_eaf_F"};
    primaryWeapon[] = {"MMG_01_black_F"};
    primaryWeaponLoadedMagazine[] = {"150Rnd_93x64_Mag"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"17Rnd_9x21_Mag",2},{"150Rnd_93x64_Mag",5}};
};

// mmg assi
class O_soldier_AAR_F : csat_baseclass {
    backpack[] = {"B_Kitbag_eaf_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_762x39_Mag_Tracer_Green_F"};
    magazines[] = {{"SmokeShell",2},{"17Rnd_9x21_Mag",2},{"30Rnd_762x39_Mag_Tracer_Green_F",7},{"150Rnd_93x64_Mag",6}};
    binocular[] = {"Binocular"};
};

class CfgCLibLoadouts {

    class BaseCollection {
        uniform[] = {};
        vest[] = {};
        headgear[] = {};
        goggle[] = {};
        backpack[] = {};
        primaryWeapon[] = {};
        primaryWeaponOptic[] = {};
        primaryWeaponMuzzle[] = {};
        primaryWeaponBarrel[] = {};
        primaryWeaponResting[] = {};
        primaryWeaponLoadedMagazine[] = {};
        secondaryWeapon[] = {};
        secondaryWeaponOptic[] = {};
        secondaryWeaponMuzzle[] = {};
        secondaryWeaponBarrel[] = {};
        secondaryWeaponResting[] = {};
        secondaryWeaponLoadedMagazine[] = {};
        handgun[] = {};
        handgunOptic[] = {};
        handgunMuzzle[] = {};
        handgunBarrel[] = {};
        handgunResting[] = {};
        handgunLoadedMagazine[] = {};
        binocular[] = {};
        magazines[] = {};
        items[] = {};
        itemsUniform[] = {};
        itemsVest[] = {};
        itemsBackpack[] = {};
        linkedItems[] = {};
        script[] = {};
        removeAllWeapons = 0;
        removeAllItems = 0;
        removeAllAssignedItems = 0;
    };
    //AAF Grundausrüstung
    class mim_baseclass : BaseCollection {
        uniform[] = {"U_I_CombatUniform","U_I_CombatUniform_shortsleeve"};
        vest[] = {"V_PlateCarrierIA1_dgtl"};
        headgear[] = {"H_HelmetIA"};
        backpack[] = {"B_AssaultPack_dgtl"};
        handgun[] = {"hgun_ACPC2_F"};
        handgunLoadedMagazine[] = {"9Rnd_45ACP_Mag"};
        binocular[] = {"Binocular"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_anprc148jem","ItemGPS","Binocular"};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    //AAF Off
    class I_officer_F : mim_baseclass {
        vest[] = {"V_TacVest_oli"};
        headgear[] = {"H_HelmetCrew_I"};
        backpack[] = {"tf_anprc155"};
        primaryWeapon[] = {"arifle_Mk20C_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",10}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
    };

    //AAF SQL
    class I_Soldier_TL_F : mim_baseclass {
		headgear[] = {"H_HelmetCrew_I"};
		vest[] = {"V_PlateCarrierIA2_dgtl"};
        backpack[] = {"tf_anprc155"};
        primaryWeapon[] = {"arifle_Mk20_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"SmokeShellRed",1},{"SmokeShellYellow",1},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",10},{"1Rnd_HE_Grenade_shell",8},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeYellow_Grenade_shell",2}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
	};

    //AAF Crewman
    class I_crew_F : mim_baseclass {
        headgear[] = {"H_HelmetCrew_I"};
        primaryWeapon[] = {"arifle_Mk20C_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",10}};
    };

    //AAF FTL
    class I_Soldier_SL_F : mim_baseclass {
		vest[] = {"V_PlateCarrierIA2_dgtl"};
        primaryWeapon[] = {"arifle_Mk20_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
		secondaryWeapon[] = {"launch_MRAWS_green_F"};
        secondaryWeaponLoadedMagazine[] = {"MRAWS_HEAT_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",10},{"1Rnd_HE_Grenade_shell",8},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeYellow_Grenade_shell",2}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
		};

    //AAF LMG
    class I_Soldier_AR_F : mim_baseclass {
        primaryWeapon[] = {"LMG_Mk200_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
		primaryWeaponResting[] = {"bipod_03_F_blk"};
        primaryWeaponLoadedMagazine[] = {"200Rnd_65x39_cased_Box_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"200Rnd_65x39_cased_Box_Tracer",3}};
    };

    //AAF Schütze AT
    class I_Soldier_AT_F : mim_baseclass {
        primaryWeapon[] = {"arifle_Mk20_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
        secondaryWeapon[] = {"launch_O_Vorona_green_F"};
        secondaryWeaponLoadedMagazine[] = {"Vorona_HEAT"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",10},{"Vorona_HEAT",1}};
    };

    // AAF Hilfsschütze AT
    class I_Soldier_AAT_F : mim_baseclass {
		backpack[] = {"B_Carryall_oli"};
        primaryWeapon[] = {"arifle_Mk20_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",10},{"Vorona_HEAT",2},{"MRAWS_HEAT_F",1}};
    };

    //AAF Grenadier
    class I_Soldier_GL_F : mim_baseclass {
		vest[] = {"V_PlateCarrierIA2_dgtl"};
        primaryWeapon[] = {"arifle_Mk20_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",10},{"1Rnd_HE_Grenade_shell",6},{"1Rnd_Smoke_Grenade_shell",5}};
    };
	
	//AAF Rifleman
    class I_soldier_F : mim_baseclass {
		backpack[] = {"B_Carryall_oli"};
        primaryWeapon[] = {"arifle_Mk20_F"};
        primaryWeaponOptic[] = {"optic_ACO"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag_Tracer_Yellow"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",6},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag_Tracer_Yellow",14},{"200Rnd_65x39_cased_Box_Tracer",1},{"1Rnd_HE_Grenade_shell"6},{"1Rnd_Smoke_Grenade_shell",6}};
		items [] = {{"ACE_EntrenchingTool",1},{"ACE_EarPlugs",1},{"ACE_morphine",6},{"ACE_fieldDressing",20},{"ACE_CableTie",6}};
    };


    //CSAT Grundausrüstung

    class csat_baseclass : BaseCollection {
        uniform[] = {"U_O_CombatUniform_ocamo"};
        vest[] = {"V_HarnessO_brn"};
        headgear[] = {"H_HelmetO_ocamo","H_HelmetSpecO_ocamo"};
        backpack[] = {"B_FieldPack_khk"};
        handgun[] = {"hgun_Rook40_F"};
        handgunLoadedMagazine[] = {"16Rnd_9x21_Mag"};
        binocular[] = {"Binocular"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
        items[] = {{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
        linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","tf_fadak","ItemGPS","Binocular"};
        removeAllWeapons = 1;
        removeAllItems = 1;
        removeAllAssignedItems = 1;
    };

    //CSAT Off
    class O_officer_F : csat_baseclass {
		vest[] = {"V_TacVest_khk"};
        headgear[] = {"H_HelmetCrew_O"};
        backpack[] = {"tf_mr3000"};
        primaryWeapon[] = {"arifle_Katiba_C_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
		};

    //CSAT SQL
    class O_Soldier_TL_F : csat_baseclass {
        headgear[] = {"H_HelmetCrew_O"};
		vest[] = {"V_HarnessOGL_brn"};
		backpack[] = {"tf_mr3000"};
        primaryWeapon[] = {"arifle_Katiba_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"SmokeShellRed",1},{"SmokeShellYellow",1},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",8},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeYellow_Grenade_shell",2}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
		};

    //CSAT Crewman
    class O_crew_F : csat_baseclass {
        headgear[] = {"H_HelmetCrew_O"};
        primaryWeapon[] = {"arifle_Katiba_C_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10}};
    };

    //CSAT FTL
    class O_Soldier_SL_F : csat_baseclass {
		vest[] = {"V_HarnessOGL_brn"};
        primaryWeapon[] = {"arifle_Katiba_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",8},{"1Rnd_Smoke_Grenade_shell",5},{"1Rnd_SmokeRed_Grenade_shell",2},{"1Rnd_SmokeYellow_Grenade_shell",2}};
		items [] = {{"ACE_MapTools",1},{"ACE_EarPlugs",1},{"ACE_morphine",1},{"ACE_fieldDressing",4},{"ACE_CableTie",2}};
		};

    //CSAT LMG
    class O_Soldier_AR_F : csat_baseclass {
        primaryWeapon[] = {"LMG_Zafir_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"150Rnd_762x54_Box"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"150Rnd_762x54_Box",4}};
    };

    //CSAT Schütze AT
    class O_Soldier_AT_F : csat_baseclass {
        primaryWeapon[] = {"arifle_Katiba_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        secondaryWeapon[] = {"launch_RPG32_F"};
        secondaryWeaponLoadedMagazine[] = {"RPG32_F"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"RPG32_F",2}};
    };

    //CSAT Hilfsschütze AT
    class O_Soldier_AAT_F : csat_baseclass {
        primaryWeapon[] = {"arifle_Katiba_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"RPG32_F",4}};
    };

    //CSAT Grenadier
    class O_Soldier_GL_F : csat_baseclass {
		vest[] = {"V_HarnessOGL_brn"};
        primaryWeapon[] = {"arifle_Katiba_GL_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",10},{"1Rnd_HE_Grenade_shell",6},{"1Rnd_Smoke_Grenade_shell",5}};
    };

    //CSAT Rifleman
    class O_soldier_F : csat_baseclass {
		backpack[] = {"B_Carryall_ocamo"};
        primaryWeapon[] = {"arifle_Katiba_F"};
        primaryWeaponOptic[] = {"optic_ACO_grn"};
        primaryWeaponLoadedMagazine[] = {"30Rnd_65x39_caseless_green_mag_Tracer"};
        magazines[] = {{"MiniGrenade",2},{"SmokeShell",6},{"16Rnd_9x21_Mag",2},{"30Rnd_65x39_caseless_green_mag_Tracer",14},{"150Rnd_762x54_Box",2},{"1Rnd_HE_Grenade_shell"6},{"1Rnd_Smoke_Grenade_shell",6}};
		items [] = {{"ACE_EntrenchingTool",1},{"ACE_EarPlugs",1},{"ACE_morphine",6},{"ACE_fieldDressing",20},{"ACE_CableTie",6}};
    };
  };